def nonstop_hotspot(area:str) -> int:
    '''
    A telefonommal szeretnék csatlakozni egy wifi hálózatra.
    Rendelkezésemre áll a környék ábrája egy string formájában (area).
    Ez a függvény visszaadja a telefonom számára elérhető wifi hálózatok számát

    >>> nonstop_hotspot("*   P  *   *")
    3

    >>> nonstop_hotspot("*  * #  * P # * #")
    1

    >>> nonstop_hotspot("***#P#***")
    0

    Args:
        area (str): 
        - P: jelképezi a telefonom pozícióját
        - *: egy elérhető wifi
        - #: akadály, amin a wifi sugarai nem tudnak áthatolni

        További szabályok
        - A "P" karakter pontan egyszer fordul elő benne
        - "*" karakter bárhol és bármennyi előfordulhat
        - "#" karakter bárhol és bármennyi előfordulhat
        - szóköz bárhol és bármennyi előfordulhat

    Returns:
        int: a telefonom számára elérhető wifi-k száma
    '''

'''
#Tesztelés
import doctest
doctest.testmod()
'''
