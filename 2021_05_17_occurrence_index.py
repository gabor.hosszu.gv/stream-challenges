def occurence_index(myList, target) -> list:
    """
    Megkeresei a target minden előfordulását a myList-ben és visszaadja a
    találatokok index-eit listába szedve.

    Args:
        list ([type]): 
        target ([type]): A keresett elem

    Returns:
        list[int]: Az elem előfordulásainak indexei listába gyűjtve
    """


# Testing starts here
test_data = [
    {
        'args' : [["a", "a", "b", "a", "b", "a"], "a"],
        'result' : [0, 1, 3, 5]
    },
    {
        'args' : [[1, 5, 5, 2, 7], 7],
        'result' : [4]
    },
    {
        'args' : [[1, 5, 5, 2, 7], 5],
        'result' : [1, 2]
    },
        {
        'args' : [[1, 5, 5, 2, 7], 8],
        'result' : []
    },
]

for data in test_data:
    assert occurence_index(*data['args']) == data["result"]

print("All test OK", end='\n\n')
