def array_diff(a:list, b:list) -> list:
    """
    Lista "kivonás.
    Elvátolítja a "b" lista összes elemét az "a" listából és visszaadja az így módosított "a" listát.
    Amennyiben "b" lista egyik eleme többször előfordul az "a" listában, az összeset eltávolítja.
    Amennyiben "b" lista egyik eleme nem fordul az "a" listában nem történik semmi.

    Args:
        a (list): A kisebbítendő lista
        b (list): A kivonandó lista

    Returns:
        list: A Különbség lista
    """

#Tesztelés
if __name__ == "__main__":
    assert array_diff([1,2],[1]) == [2], "Nem Oké"
    assert array_diff([1,2],[]) == [1,2], "Nem Oké"
    assert array_diff([1,2,2,2,3],[2]) == [1,3], "Nem Oké"
    assert array_diff([1,2,2,2,3],[2,4]) == [1,3], "Nem Oké"
    assert array_diff([1,2,2,2,3],[1,4]) == [2,2,2,3], "Nem Oké"
    print("Minden teszt OK")
