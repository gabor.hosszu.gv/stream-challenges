d = {'I':1, 'V':5, 'X':10, 'L':50, 'C':100, 'D':500, 'M':1000}
noteable = {'IV':4 , 'IX':9, 'XL':40, 'XC':90, 'CD':400, 'CM':900}

def roman_to_int1(roman):
    sum = 0
    for prev, curr in zip(roman,roman[1:]):
        if d[curr] > d[prev]:
            sum -= d[prev]
        else:
            sum += d[prev]
    sum += d[roman[-1]]
    return sum


def roman_to_int2(roman):
    sum = 0
    for n in noteable:
        if n in roman:
            sum += noteable[n]
            roman = roman.replace(n, '')
            continue
    
    for c in roman:
        sum += d[c]
    return sum
