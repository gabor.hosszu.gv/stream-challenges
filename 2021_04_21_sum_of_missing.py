def sum_of_missing(nums:list) -> int:
    """
    Visszaadja a nums listából hiányzó számok szummáját.

    >>> sum_of_missing([4, 3, 8, 1, 2])
    18

    >>> sum_of_missing([17, 16, 15, 10, 11, 12])
    27

    >>> sum_of_missing([1, 2, 3, 4, 5])
    0

    Args:
        nums (list): egy integerekből álló lista. A feladat szempontjából ez egy rendezetlen, hiányos számsor

    Returns:
        int: a hiányzó számok összege
    """

"""
#Tesztelés
import doctest
doctest.testmod()
"""