import re

def decode_namecard1(encoded_string: str):
    #grindy
    return re.match(r'(?P<first_name>\D+)0+(?P<last_name>\D+)0+(?P<id>\d+)', encoded_string).groupdict()

def decode_namecard2(encoded_string: str):
    #slapec
    def get_next_text():
        text = ''

        def _next_text():
            if text:
                yield text

        for c in encoded_string:
            if c == '0':
                yield from _next_text()
                text = ''
            else:
                text += c

        yield from _next_text()

    return dict(zip(['first_name', 'last_name', 'id'], get_next_text()))

def decode_namecard3(encoded_string: str):
    #Dwarf1
    m = re.findall("\D+", encoded_string)
    k = encoded_string.rfind("0")
    nevjegy  = {
    "first_name": m[0],
    "last_name": m[1],
    "id": encoded_string[k+1:]
    }
    return nevjegy

def decode_namecard4(encoded_string: str) :
    #Dwarf2
    k = encoded_string.find("0")
    z = encoded_string.rfind("0")
    nevjegy  = {
    "first_name": encoded_string[:k],
    "last_name": encoded_string[k+1:z].strip("0"),
    "id": encoded_string[z+1:]
    }
    return nevjegy

def decode_namecard(encoded_string: str):
    #Daxolon
    data = encoded_string.split('0')
    while('' in data) :
        data.remove('')
    result = {
        'first_name': str(data[0]),
        'last_name': str(data[1]),
        'id': str(data[2])
    }
    return result

# Testing starts here
test_data = [
{
    'args': ['John000Doe000123'],
    'result': {"first_name": "John","last_name": "Doe","id": "123"}
},
{
    'args': ['michael0smith004331'],
    'result': {"first_name": "michael", "last_name": "smith", "id": "4331"}
},
{
    'args': ['Thomas00LEE00000403'],
    'result': {"first_name": "Thomas", "last_name": "LEE", "id": "403"}
},
]


for data in test_data:
    assert decode_namecard(*data['args']) == data["result"]

print("All test OK", end='\n\n')

"""# Autotest
import inspect
import timeit

g = globals().copy()
solutions = {}
for item in g:
    if hasattr(g[item], '__call__'):
        solutions[inspect.getsource(g[item]).split('\n')[1].replace('#', '').strip()] = g[item]


timeit_results = {}
for data in test_data:
    key = ''.join(str(data['args']))
    timeit_results[key] = []
    print(f'Test data: {key}')
    for coder in solutions:
        print(f'{coder} ', end='')
        timeit_command = f'{str(solutions[coder].__name__)}('
        timeit_command += ', '.join(f'"{arg}"' if type(arg) is str else str(arg) for arg in data["args"])
        timeit_command += ')'
        run_time = timeit.timeit(timeit_command, globals=globals())
        print(run_time)
        timeit_results[key].append({'coder': coder, 'time': run_time})
    timeit_results[key].sort(key=lambda x: x['time'])
    print('')


print('\nResults in order', end='')
for i in timeit_results:
    print(f'\nTest data: {i}')
    for i, result in enumerate(timeit_results[i]):
        print(f'{i+1}. {result["coder"]} : {result["time"]}', end='\n')"""
