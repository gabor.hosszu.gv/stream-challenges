def word_split(strList:list) -> str:
    """
    Vizsgálja, hogy a strList[0] string felbontható-e két string kombinációjára a strList[1]-ból. Amennyiben igen, visszaadja a két substringet vesszővel elválasztva
    (a sorrendre figyelve). Amennyiben nincs megoldás "not possible"-t ad vissza.

    Példák:

    >>> word_split(["baseball", "a,all,b,ball,bas,base,cat,code,d,e,quit,z"])
    'base,ball'
    >>> word_split(["abcgefd", "a,ab,abc,abcg,b,c,dog,e,efd,zzzz"])
    'abcg,efd'
    >>> word_split(["abcgefdqqqq", "a,ab,abc,abcg,b,c,dog,e,efd,zzzz"])
    'not possible'

    Args:
        strList (list): két elemes lista ahol:
            strList[0] - a felbontásra váró string
            strList[1] - a lehetséges rész-stringek vesszővel elválasztva

    Returns:
        str: amennyiben a felbontásra váró szó előállítható a rész-stringekből, akkor a rész-stringek vesszővel elválasztva. Más esetben "not possible".
    """
