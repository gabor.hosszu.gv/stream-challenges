from typing import Dict

def decode_namecard(encoded_string: str) -> Dict:
    """
    A kapott stringed dict-be tördelve adja vissza.

    A bemenő encoded_string egy értelmezésre szoruló névjegykártya.
    Három elemet tartalmaz és ez a három mindig jelen van:
    - First name
    - Last name
    - id
    Ezek az elemek legalább egy "0"-val vannak elválasztva, de lehet bármennyi.
    Az id, soha nem kezdődik "0"-val.

    Példa a működésre:
        "michael0smith004331" ➞ {"first_name": "michael", "last_name": "smith","id": "4331"}

    Args:
        encoded_string: A szétválasztandó string

    Returns:
        Dict: névjegykártya a következő struktúra szerint:
            {
                "first_name": str,
                "last_name": str,
                "id": str
            }
    """

# Testing starts here
test_data = [
{
    'args': ['John000Doe000123'],
    'result': {"first_name": "John","last_name": "Doe","id": "123"}
},
{
    'args': ['michael0smith004331'],
    'result': {"first_name": "michael", "last_name": "smith", "id": "4331"}
},
{
    'args': ['Thomas00LEE0000043'],
    'result': {"first_name": "Thomas", "last_name": "LEE", "id": "43"}
},
]

for data in test_data:
    assert decode_namecard(*data['args']) == data["result"]

print("All test OK", end='\n\n')