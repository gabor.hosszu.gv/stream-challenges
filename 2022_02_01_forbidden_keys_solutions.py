def is_forbidden_keys_1(data: dict, forbidden_words: list) -> bool:
    # Gridy
    return True if set(data.keys()).intersection(forbidden_words) else False

def is_forbidden_keys_2(data: dict, forbidden_words: list) -> bool:
    # Iseroo
    for x in data.keys():
        if x in forbidden_words:
            return True
    return False

def is_forbidden_keys_3(data: dict, forbidden_words: list) -> bool:
    # HBXSZA
    return bool(set(forbidden_words) & set(data))

def is_forbidden_keys_4(data: dict, forbidden_words: list) -> bool:
    # "Máté#2749"
    for key in data.keys():
        if(key in forbidden_words):
            return True
    else:
        return False

def is_forbidden_keys(data: dict, forbidden_words: list) -> bool:
    # HBXSZA2
    for word in forbidden_words:
        if word in data:
            return True
    return False

# Testing starts here
test_data = [
    {
    'args': [{"alma": 1}, [1]],
    'result': False
},
{
    'args': [{"alma": 1}, ["alma"]],
    'result': True
},
{
    'args': [{"alma": 1, "körte": "h"}, ["tej", 1, 2, "szalámi", "h", "péntek"]],
    'result': False
},
{
    'args': [{"alma": 1, "körte": "h"}, ["tej", 1, 2, "szalámi", "h", "péntek", "alma"]],
    'result': True
},
{
    'args': [{"alma": 1, "körte": "h", "narancs": 3.2}, ["tej", "sajt", "narancs"]],
    'result': True
},
{
    'args': [{"alma": 1, "körte": "h", "narancs": 3.2, "uborka": "péntek", "banán": 2}, ["péntek"]],
    'result': False
},
{
    'args': [{"alma": 1, "körte": "h", "narancs": 3.2, "uborka": "péntek", "banán": 2}, ["banán"]],
    'result': True
},
{
    'args': [{"alma": 1, "körte": "h", "narancs": 3.2, "uborka": "péntek", "banán": 2}, ["tej", 1, 2, "szalámi", "h", "péntek", "banán"]],
    'result': True
},
]