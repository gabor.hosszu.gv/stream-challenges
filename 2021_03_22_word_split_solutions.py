#by slapec
def word_split(str_list: list) -> str:
    what, where = str_list
    substrings = set(where.split(','))
    for i in range(1, len(what)):
        pair = {what[:i], what[i:]}
        if pair & substrings == pair:
            return what[:i] + ',' + what[i:]

    else:
        return 'not possible'


#by grindy
def word_split(str_list:list) -> str:
    substrings=str_list[1].split(',')
    for ss in substrings:
        if ss in str_list[0]:
            if (rest:=str_list[0].replace(ss, '')) in substrings:
                return f"{ss},{rest}" if ss+rest == str_list[0] else f"{rest},{ss}"
    return 'not possible'


#by kolt
from itertools import combinations

def word_split(strList:list) -> str:
    for items in combinations(strList[1].split(','),2):
        if items[0] + items[1] == strList[0]:
            return(items[0] + ',' + items[1])
        if  items[1] + items[0] == strList[0]:
            return(items[1] + ',' + items[0])
    return('not possible')

#by attyhor
def word_split(strList:list) -> str:
    substrings = strList[1].split(",")
    for i in range(len(substrings)):
        if substrings[i] == strList[0][:len(substrings[i])]:
            maradek = strList[0][len(substrings[i]):]
            if maradek in substrings:
                valasz = f"{substrings[i]}, {maradek}"
                return valasz
    return "not possible"
