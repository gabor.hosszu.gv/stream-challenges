
def sum_frac_1(fracts):
    # grindy
    from fractions import Fraction
    result = sum([Fraction(x[0],x[1]) for x in fracts])
    return [result.numerator, result.denominator] if result.numerator%result.denominator != 0 else result.numerator//result.denominator

def sum_frac_2(fracts: list[list[int,int]]):
    # dwarf I
    import math
    szamlalo = 0
    nevezo = 1
    for tort in fracts:
        nevezo = nevezo * tort[1]
    for tort in fracts:
        szamlalo = szamlalo+tort[0]*nevezo/tort[1]
    oszto = math.gcd(int(szamlalo), nevezo)
    if szamlalo % nevezo:
        return ([int(szamlalo/oszto), int(nevezo/oszto)])
    else:
        return (int(szamlalo/nevezo))
    
def sum_frac_3(fracts: list[list[int,int]]):
    # dwarf II
    import math
    szamlalok = []
    nevezo = 1
    for tort in fracts:
        szamlalok = [szamlalo * tort[1] for szamlalo in szamlalok]
        szamlalok.append(tort[0] * nevezo)
        nevezo = nevezo * tort[1]
    szamlalo = sum(szamlalok)
    oszto = math.gcd(szamlalo, nevezo)
    if szamlalo % nevezo:
        return ([int(szamlalo/oszto), int(nevezo/oszto)])
    else:
        return (int(szamlalo/nevezo))

def sum_frac_4(fracts: list[list[int,int]]):
    # dwarf III
    import math
    szamlalo = 0
    nevezo = 1
    for tort in fracts:
        szamlalo = szamlalo * tort[1] + tort[0] * nevezo
        nevezo = nevezo * tort[1]
    oszto = math.gcd(szamlalo, nevezo)
    if szamlalo % nevezo:
        return ([int(szamlalo/oszto), int(nevezo/oszto)])
    else:
        return (int(szamlalo/nevezo))

def sum_frac_5(fracts: list[list[int,int]]):
    # dwarf IV
    import math
    if len(fracts)==2:
        szamlalo = fracts[0][0] * fracts[1][1] + fracts[0][1] * fracts[1][0]
        nevezo = fracts[1][1]*fracts[0][1]
        oszto = math.gcd(szamlalo, nevezo)
        if szamlalo % nevezo:
            return ([int(szamlalo/oszto), int(nevezo/oszto)])
        else:
            return (int(szamlalo / nevezo))
    else:
        temp = sum_frac_5(fracts[1:])
        if isinstance(temp, int):
            temp = [temp, 1]
        szamlalo = fracts[0][0] * temp[1] + fracts[0][1] * temp[0]
        nevezo = temp[1]*fracts[0][1]
        oszto = math.gcd(szamlalo, nevezo)
        if szamlalo % nevezo:
            return ([int(szamlalo/oszto), int(nevezo/oszto)])
        else:
            return (int(szamlalo/nevezo))


def sum_frac_6(fracts: list[list[int, int]]):
    # HBXSZA
    from math import gcd
    
    num = 0
    denom = 1
    
    for fract in fracts:
        curr_denom = fract[1]
        num = num * curr_denom + fract[0] * denom
        denom = denom * curr_denom 
    
    if not num % denom:
        return num // denom
    
    gcd_ = gcd(num, denom)
    return [num // gcd_, denom // gcd_]

def sum_frac_8(fracts):
    # Máté#2749
    import math
    num = fracts[0][0]
    den = fracts[0][1]
    for numerator, denominator in fracts[1:]:
        if(denominator == den):
            num += numerator
        elif(den % denominator == 0):
            num += (numerator*(den/denominator))
        else:
            num *= denominator
            numerator *= den
            num += numerator
            den *= denominator
    gcd = math.gcd(int(num), int(den))
    result = (num/gcd) / float(den/gcd)
    if result % 1 != 0:
        return [int(num/gcd), int(den/gcd)]
    return result


def sum_frac_9(fracts):
    # danixx I
    from fractions import Fraction
    import itertools
    consts = list(itertools.chain(*fracts))
    result = 0

    for i in range(0, len(consts), 2):
        result += Fraction(consts[i], consts[i+1])

    res = Fraction(result)

    return res.numerator if res.denominator == 1 else [res.numerator, res.denominator]


def sum_frac_10(fracts):
    # danixx II
    import itertools
    from gmpy2 import mpq
    consts = list(itertools.chain(*fracts))
    result = 0

    for i in range(0, len(consts), 2):
        result += mpq(consts[i], consts[i+1])

    res = mpq(result)

    return res.numerator if res.denominator == 1 else [res.numerator, res.denominator]