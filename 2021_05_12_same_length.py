def same_length(text:str) -> bool:
    """
    Vizsgálja, hogy a kapott string-ben az egymást követő 1-esek hossza megegyezik-e az utána következő,
    egymást követő 0-ák hosszával. Ha igen True-t ad vissza, minden más esetben False.

    Args:
        text (str): Mindig egyessel kezdődik. Csak 1-esket és 0-ákat tartalmazhat. 

    Returns:
        bool
    """

# Testing starts here
test_data = [
    {
        'args' : ["110011100010"],
        'result' : True
    },
    {
        'args' : ["101010110"],
        'result' : False
    },
    {
        'args' : ["111100001100"],
        'result' : True
    },
        {
        'args' : ["111"],
        'result' : False
    },
]

for data in test_data:
    assert same_length(*data['args']) == data["result"]

print("All test OK", end='\n\n')
