def anti_list2(list1:list, list2:list) -> bool:
    #Dwarf
    elem_1 = (list1[0],type(list1[0]))
    elem_2 = (list2[0],type(list2[0]))
    if elem_1 == elem_2:
        return False
    else:
        for sorszam in range(1, len(list1)):
            if (list1[sorszam], type(list1[sorszam]))==(list2[sorszam], type(list2[sorszam])) or not (((list1[sorszam], type(list1[sorszam])) == elem_1 and (list2[sorszam], type(list2[sorszam])) == elem_2) or ((list1[sorszam], type(list1[sorszam])) == elem_2 and (list2[sorszam], type(list2[sorszam])) == elem_1)):
                return False
    return True

def anti_list6(list1: list, list2: list) -> bool:
    #slapec
    if len(list1) == len(list2):
        unique = set()
        for left, right in zip(list1, list2):
            left, right = (type(left), left), (type(right), right)
            if left == right:
                return False
            unique.update((left, right))
        return len(unique) == 2
    return False

def anti_list7(list1:list, list2:list) -> bool:  
    #HBXSZA
    
    if len(list1) != len(list2) or len(list1) < 1:
        return False

    def is_eq(x, y):
        return x == y and type(x) == type(y)
        
    elem1 = list1[0]
    elem2 = list2[0]
    
    if is_eq(elem1, elem2):
        return False
    
    for l1, l2 in zip(list1, list2):
        if is_eq(l1, elem1):
            if is_eq(l2, elem2):
                continue
        
        if is_eq(l1, elem2):
            if is_eq(l2, elem1):
                continue

        return False
    
    return True

# Testing starts here
test_data = [
    {
     'args' : [["1", "0", "0", "1"], ["0", "1", "1", "0"]],
     'result' : True
    },
    {
     'args' : [["apples", "bananas", "bananas"], ["bananas", "apples", "apples"]],
     'result' : True
    },
    {
     'args' : [[3.14, True, 3.14], [3.14, False, 3.14]],
     'result' : False
    },
    {
    'args': [[1, 1], [3, 4]],
    'result': False
    },
    {
    'args': [[1, 1], [0, 0]],
    'result': True
    },
    {
    'args': [[1, True], [True, 1]],
    'result': True
    },
    {
    'args': [[1, True, 1.0], [False, False, False]],
    'result': False
    }
]

for data in test_data:
    print(data)
    assert anti_list(*data['args']) == data["result"]

print("All test OK", end='\n\n')