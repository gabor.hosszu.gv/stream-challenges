def find_outlier_1(integers: list[int]) -> int:
    # grindy
    odd = sum(map(lambda x: x%2, integers[:3])) > 1

    if odd == 1 or odd == 2:
        for i in integers[:3]:
            if i%2 != odd:
                return i
    for i in integers:
        if i%2 != odd:
            return i

def find_outlier_2(integers: list[int]) -> int:
    # dwarf I
    parosak = 0
    paratlanok = 0
    for szam_index in range(len(integers)):
        if integers[szam_index] % 2:
            paratlanok += 1
            paratlan_utolso_index = szam_index
        else:
            parosak += 1
            paros_utolso_index = szam_index
        if parosak >= 2 and paratlanok:
            return (integers[paratlan_utolso_index])
        if paratlanok >= 2 and parosak:
            return (integers[paros_utolso_index])
    
def find_outlier_3(integers: list[int]) -> int:
    # dwarf II
    if len(integers) % 2:
        keresett = sum(integers) % 2
    else:
        if sum(integers[:-1]) % 2:
            keresett = not (integers[-1] % 2)
        else:
            return (integers[-1])
    for szam in integers:
        if szam % 2 == keresett:
            return (szam)

def find_outlier_4(integers: list[int]) -> int:
    # HBXSZA I
    first = integers[0]
    second = integers[1]
    
    first_parity = first % 2
    second_parity = second % 2
    
    if first_parity == second_parity:
        for integer in integers[2:]:
            if integer % 2 != first_parity:
                return integer
            
    if first_parity == integers[2] % 2:
        return second
    
    return first

def find_outlier_5(integers):
    # Máté#2749
    for index in range(1, len(integers)):
        if (integers[index] & 1) != (integers[index - 1] & 1):
            try:
                if (integers[index] & 1) != (integers[index + 1] & 1):
                    return integers[index]
            except:
                return integers[index]
    else:
        return integers[0]

def find_outlier_6(integers: list[int]) -> int:
    # HBXSZA II
    return x[0] if (x := sorted(integers, key=(1).__and__))[1] % 2 else x[-1]

def find_outlier_7(integers: list[int]) -> int:
    # dwarf III
    keresett = not (integers[0] % 2 + integers[1] % 2 + integers[2] % 2 > 1)
    for szam in integers:
        if szam % 2 == keresett:
            return szam

from itertools import groupby
def find_outlier_8(integers: list[int]) -> int:
    # __Hels__
    even = None
    odd = None
    group_obj = groupby(integers, key=lambda x: x%2 == 0)
    for key, value in group_obj:
        if key:
            even = list(value)
        else:
            odd = list(value)
    if len(even) > len(odd):
        for num in odd:
            return num
    else:
        for num in even:
            return num

def find_outlier_9(integers: list[int]) -> int:
    # kolt
    odd_count = 0
    even_count = 0
    first_odd = 'N'
    first_even = 'N'

    for integer in integers:
        if integer % 2 == 0:
            even_count += 1
            if even_count == 1:
                first_even = integer
                continue
            elif first_odd != 'N':
                return first_odd
        else:
            odd_count += 1
            if odd_count == 1:
                first_odd = integer
                continue
            elif first_even != 'N':
                return first_even

    if odd_count > even_count:
        return first_even
    else:
        return first_odd

def find_outlier_10(integers: list[int]) -> int:
    # dwarf IV
    if len(integers) == 3:
        if integers[0]%2 == integers[1]%2:
            return integers[2]
        else:
            return (integers[integers[0]%2 == integers[2]%2])
    else:
        hossz = len(integers)//2
        hossz = hossz - (hossz % 2)
        if sum(integers[:hossz]) % 2:
            return (find_outlier_10(integers[:hossz+1]))
        elif (sum(integers[-hossz:]) % 2):
            return (find_outlier_10(integers[-hossz-1:]))
        else:
            if len(integers) == 2*hossz+2:
                return (find_outlier_10(integers[hossz - 1:-hossz]))
            elif len(integers) == 2*hossz+1:
                return (integers[hossz])
            else:
                return (find_outlier_10(integers[hossz:-hossz]))