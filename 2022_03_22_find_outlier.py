def find_outlier(integers: list[int]) -> int:
    """
    A kapott lista mindig vagy páros vagy páratlan számokból áll egy kivételével.
    Ez a függvény megkeresi és visszaadja a kakukktojást.

    Példa:
    [2, 4, 6, 8, 10, 3] => 3
    [160, 3, 1719, 19, 11, 13, -21] => 160

    Args:
        integers [list[int]]: legalább 3 számból álló lista

    Returns:
        int: a kakukktojás
    """
    
# Testing starts here
test_data = [
    {
        'args': [[2, 4, 6, 8, 10, 3]],
        'result': 3
    },
    {
        'args': [[160, 3, 1719, 19, 11, 13, -21]],
        'result': 160
    },
    {
        'args': [[2, 4, 0, 100, 4, 11, 2602, 36]],
        'result': 11
    },
    {
        'args': [[-34, -42, -48, -75, -44, -60, 72, -38, 86, 8, -80]],
        'result': -75
    },
    {
        'args': [[20, -27, -20, 62, -72, -44, 64, -34, 96, 70, -94, -40, -28, -30, -42, 44]],
        'result': -27
    },
    {
        'args': [[-46, 30, -20, 2, 9, 42, 8, -84, -42, -60, 20, 46, -82, -32, -18, -36, 50, 10, -38, -12, -24]],
        'result': 9
    },
    {
        'args': [[-20, -18, -24, -2, 38, 66, 84, 18, 24, -96, -14, -26, -46, 58, 52, -19, 50, -6, -38, -76, -86, -32, 82, -88, 48, 22, 8, 0, -94, 56, 64, -40, 40, 10, -74, 72, 94, -78, -42, 62, -48, 96, 86, -80, 68, -44, -60, 54, -36, -58, -30]],
        'result': -19
    },
]

for data in test_data:
    assert find_outlier(*data['args']) == data["result"]

print("All test OK", end='\n\n')
