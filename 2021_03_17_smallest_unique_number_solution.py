def smallest_unique_number(numbers: list) -> int:
    numbers.sort()
    for i in range(len(numbers)):
        if i > 0:
            if numbers[i] == numbers[i-1]:
                continue
        if i < len(numbers)-1:
            if numbers[i] == numbers[i+1]:
                continue
        return numbers[i]
    return -1

print(smallest_unique_number([9, 9, 8, 8, 7, 7, 6, 5, 5, 4, 3, 3, 12, 2, 2, 31, 1, 2, 1]))

# Ezt a megoldást kolt#0798 küldte be és a tesztjeim alapján még gyorsabb mint a fenti megoldás
def smallest_unique_number2(numlist):
    numlist.sort()
    numlist.insert(0,-1)
    numlist.append(-2)
    result=-1
    for index,num in enumerate(numlist[1:-1]): 
        if num != numlist[index] and num != numlist[index+2]:
            result=num
            break
    return result

print(smallest_unique_number2([9, 9, 8, 8, 7, 7, 6, 5, 5, 4, 3, 3, 12, 2, 2, 31, 1, 2, 1]))
