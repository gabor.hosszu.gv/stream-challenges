import re
parretn = r'[^#]*P[^#]*'

#by grindy
def nonstop_hotspot1(area:str) -> int:
    '''
    A telefonommal szeretnék csatlakozni egy wifi hálózatra.
    Rendelkezésemre áll a környék ábrája egy string formájában (area).
    Ez a függvény visszaadja a telefonom számára elérhető wifi hálózatok számát

    >>> nonstop_hotspot("*   P  *   *")
    3

    >>> nonstop_hotspot("*  * #  * P # * #")
    1

    >>> nonstop_hotspot("***#P#***")
    0

    Args:
        area (str): 
        - P: jelképezi a telefonom pozícióját
        - *: egy elérhető wifi
        - #: akadály, amin a wifi sugarai nem tudnak áthatolni

        További szabályok
        - A "P" karakter pontan egyszer fordul elő benne
        - "*" karakter bárhol és bármennyi előfordulhat
        - "#" karakter bárhol és bármennyi előfordulhat
        - szóköz bárhol és bármennyi előfordulhat

    Returns:
        int: a telefonom számára elérhető wifi-k száma
    '''

    return re.search(parretn, area)[0].count("*")

#by grindy
def nonstop_hotspot2(area:str) -> int:
    '''
    A telefonommal szeretnék csatlakozni egy wifi hálózatra.
    Rendelkezésemre áll a környék ábrája egy string formájában (area).
    Ez a függvény visszaadja a telefonom számára elérhető wifi hálózatok számát

    Args:
        area (str): 
        - P: jelképezi a telefonom pozícióját
        - *: egy elérhető wifi
        - #: akadály, amin a wifi sugarai nem tudnak áthatolni

        További szabályok
        - A "P" karakter pontan egyszer fordul elő benne
        - "*" karakter bárhol és bármennyi előfordulhat
        - "#" karakter bárhol és bármennyi előfordulhat
        - szóköz bárhol és bármennyi előfordulhat

    Returns:
        int: a telefonom számára elérhető wifi-k száma
    '''

    count = 0
    p_found = False
    for c in area:
        if c == "*":
            count += 1
        elif c == "#":
            if p_found:
                return count
            else:
                count = 0
        elif c == "P":
            p_found = True
    return count

#by slapec
def nonstop_hotspot3(area: str) -> int:
    # Megkeressük a P-t
    idx_p = area.index('P')

    # Megkeressük a P bal oldalához legközelebbi falat
    idx_wall_left = area.rfind('#', 0, idx_p)

    # Megkeressük a P jobb oldalához legközelebbi falat
    idx_wall_right = area.find('#', idx_p)

    # Jön a * számolgatás:
    # Ha a jobb oldalon egyáltalán nem volt fal
    if idx_wall_right == -1:
        # És a bal oldalon se volt egyáltalán fal
        if idx_wall_left == -1:
            # Akkor az egész stringben számoljuk a csillagokat
            return area.count('*')
        else:
            # Ha a bal oldalon volt fal (de a jobb oldalon nem)
            # Akkor a bal faltól a string végéig számolunk
            return area[idx_wall_left:].count('*')

    else:
        # Ha volt fal a bal- és a jobb oldalon is, akkor csak e két fal
        # közt végezzük a csillagok számolását
        return area[idx_wall_left:idx_wall_right].count('*')

#by zxld
def nonstop_hotspot4(area:str) -> int:
    starlist = [[star for star in item if star == "*"] for item in area.split("#") if "P" in item]
    return len(starlist[0])

#by attyhor
def nonstop_hotspot5(area:str) -> int:
    hotsp_number = 0
    pos = area.find("P")
    count = pos
    if count > 0:
        count -= 1
        while area[count] != "#":
            if area[count] == "*":
                hotsp_number += 1
            if count == 0:
                break
            count -=1
    count = pos
    if count < len(area)-1:
        count += 1
        while area[count] != "#":
            if area[count] == "*":
                hotsp_number += 1
            if count == len(area)-1:
                break
            count +=1
    return hotsp_number

#by Dwarf
def nonstop_hotspot6(area:str) -> int:
    return re.search(r"#?[*\s]*P[*\s]*#?", area).group().count("*")

#by Dwarf
def nonstop_hotspot7(area:str) -> int:
    hotspotok = 0
    jobb_oldal = False
    for char in area:
        if char == "*":
            hotspotok +=1
        elif char == "#":
            if jobb_oldal:
                break
            else:
                hotspotok = 0
        elif char == "P":
            jobb_oldal =True
    return hotspotok

#by Iseroo
def nonstop_hotspot8(area:str) -> int:
    P_index = area.index("P")
    right = area.find("#", P_index)
    left = area.rfind("#", 0, P_index)
    
    if right == -1:
        if left == -1:
            return area.count("*")
        else:
            return area[left:].count("*")
    else:
        return area[left:right].count("*")

# by zxld
def nonstop_hotspot9(area:str) -> int:
    starlist = [items for items in area.split("#") if "P" in items]
    return starlist[0].count("*")

#by papi1992
def nonstop_hotspot10(area: str) -> int:
    barrier = "#"
    hotspot = "*"

    def _calculate_hotspots(sub_area: str):
        _inner_counter = 0
        for c in sub_area:
            if c == barrier:
                break
        
            if c == hotspot:
                _inner_counter += 1

        return _inner_counter

    hotspot_counter = 0
    phone_index = area.index("P")

    hotspot_counter += _calculate_hotspots(area[phone_index:])
    hotspot_counter += _calculate_hotspots(area[:phone_index][::-1])

    return hotspot_counter

#by hex4bin
def nonstop_hotspot11(area: str) -> int:
    return [elem for elem in area.split("#") if "P" in elem][0].count("*")

#Tesztelés
"""import doctest
doctest.testmod()"""
