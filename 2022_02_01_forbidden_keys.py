def is_forbidden_keys(data: dict, forbidden_words: list) -> bool:
    """
    Validálja a data kulcsait, hogy tartalmaz-e olyat, ami megtalálható forbidden_words listában.
    Amennyiben data kulcsai igen True-t ad vissza, egyébként False

    Példa a működésre:
        1. példa
        data = {"alma": 1, "körte": "h", "narancs": 3.2}
        forbidden_words = ["tej", "sajt", "kenyér"]
            => False
        
        2. példa
        data = {"alma": 1, "körte": "h", "narancs": 3.2}
        forbidden_words = ["tej", "sajt", "narancs"]
            => True


    Args:
        data [dict]: Beérkező, validálásra váró adat
        forbidden_words [list]: A tiltott kulcsok listája

    Returns:
        bool

    """

# Testing starts here
test_data = [
    {
    'args': [{"alma": 1}, [1]],
    'result': False
},
{
    'args': [{"alma": 1}, ["alma"]],
    'result': True
},
{
    'args': [{"alma": 1, "körte": "h"}, ["tej", 1, 2, "szalámi", "h", "péntek"]],
    'result': False
},
{
    'args': [{"alma": 1, "körte": "h"}, ["tej", 1, 2, "szalámi", "h", "péntek", "alma"]],
    'result': True
},
{
    'args': [{"alma": 1, "körte": "h", "narancs": 3.2}, ["tej", "sajt", "narancs"]],
    'result': True
},
{
    'args': [{"alma": 1, "körte": "h", "narancs": 3.2, "uborka": "péntek", "banán": 2}, ["péntek"]],
    'result': False
},
{
    'args': [{"alma": 1, "körte": "h", "narancs": 3.2, "uborka": "péntek", "banán": 2}, ["banán"]],
    'result': True
},
{
    'args': [{"alma": 1, "körte": "h", "narancs": 3.2, "uborka": "péntek", "banán": 2}, ["tej", 1, 2, "szalámi", "h", "péntek", "banán"]],
    'result': True
},
]

for data in test_data:
    assert is_forbidden_keys(*data['args']) == data["result"]

print("All test OK", end='\n\n')
