def custom_size_chessboard_1(number: int) -> list[list]:
    # grindy
    board=[]
    for i in range(number):
        row = []
        for j in range(number):
            row.append((i+j)%2)
        board.append(row)
    return board

def custom_size_chessboard_2(number: int) -> list[list]:
    # grindy 2
    return [[(i+j)%2 for j in range(number)] for i in range(number)]

def custom_size_chessboard_3(number: int) -> list[list]:
    # grindy 3
    return TABLE[:number]

def custom_size_chessboard_4(number: int) -> list[list[int]]:
    # slapec
    # Az alap ötlet az, hogy mindig ugyan az a két tömb ismétlődik.
    # Tehát első lépésben előállítjuk a két megfelelő elemszámú tömbünket,
    # majd ezt a párost ismételtetjük. Szerencsére a list objektumokon a *
    # operátor pontosan ezt valósítja meg.
    # Plusz segítség, hogy a két ismétlődő tömbben található két elem is ismétlődik
    # pont úgy, mint ahogy maguk a tömbök, azaz egyetlen algoritmus megoldja az egész
    # feladatot.
    # Oda kell figyelni, hogy mivel kételemű tömböket szorzunk, kétszer hosszabb
    # eredményt kapunk, mint amire szükségünk van. A felesleges elemeket lazán csak levágjuk.
    # A visszatérési értéknek tömbnek kell lennie, ezt a [*...] szintaxis biztosítja. Ez esetben
    # a * már nem szorzást valósít meg, hanem az argumentum lista kiterjesztését.

    return [*((
                ([0, 1] * number)[:number],
                ([1, 0] * number)[:number]
            ) * number)[:number]]


def custom_size_chessboar_5(number):
    # Máté-1
    return [list((j+i)%2 for j in range(number)) for i in range(number)]

def custom_size_chessboard_6(number: int) -> list[list[int]]:
    #HBXSZA
    q, r = divmod(number, 2)
    
    if not r:
        return [[0, 1] * q, [1, 0] * q] * q
    
    base = [0, 1] * q
    base.append(0)
    
    base_2 = [1, 0] * q
    base_2.append(1)
    
    ret = [base, base_2] * q
    ret.append(base)
    
    return ret

def custom_size_chessboard_7(number):
    # Máté-2
    l = [i%2 for i in range(number)]
    arr = []
    for line in range(number):
        arr.append(l)
        l = [int(not i) for i in l]
    return arr

def custom_size_chessboard_8(number: int) -> list[list[int]]:
    # Dwarf [I]
    sor0 = [0, 1]
    while len(sor0)<number+1:
        sor0.extend(sor0)
    sor1 = sor0[1:number+1]
    sor0 = sor0[:number]
    tabla = [sor0, sor1]
    while len(tabla)<number+1:
        tabla.extend(tabla)
    return (tabla[:number])

def custom_size_chessboard_9(number: int):
    # Dwarf [II]
    paratlan = number%2
    felezett=number>>1
    if paratlan:
        n = int(5 * 4 ** (felezett - 1) + (4 ** (felezett - 1) - 1) / 3)
    else:
        n = int(2*4**(felezett-1)+2*(4**(felezett-1)-1)/3)
    sor1 = [int(x) for x in bin(n).replace("0b", "")]
    sor0 = [0] + sor1[:-1]
    tabla = [sor0, sor1]
    while len(tabla)<number+1:
        tabla.extend(tabla)
    return (tabla[:number])