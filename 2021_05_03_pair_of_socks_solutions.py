import collections

#by grindy
def pair_of_socks1(text:str) -> int:
    c = collections.Counter(text)
    return sum([c[x] // 2 for x in c])

#by grindy
def pair_of_socks2(text:str) -> int:
    d = {}
    result = 0
    for c in text:
        if c in d:
            if d[c] + 1 == 2:
                result += 1
                d[c] = 0
            else:
                d[c] += 1
        else:
            d[c] = 1
    return result
            
#by HBXSZA
def pair_of_socks3(text:str) -> int:
    return sum(val // 2 for val in collections.Counter(text).values())

#by slapec
import itertools

def pair_of_socks4(text:str) -> int:

    text = sorted(text)
    total = 0
    for _, population in itertools.groupby(text):  # Ha már zokni párokról beszélünk
        total += len(list(population)) // 2
    return total

#by Dwarf
def pair_of_socks5(text:str) -> int:
 
    zokni_szotar = {}
    for zokni in text:
        if zokni in zokni_szotar:
            zokni_szotar[zokni] += 1
        else:
            zokni_szotar[zokni] = 1
    osszes_par = 0
    for ertek in zokni_szotar.values():
        osszes_par += ertek>>1
    return osszes_par

#by Dwarf
def pair_of_socks6(text:str) -> int:
    zokni_szotar = {}
    for zokni in text:
        try:
            zokni_szotar[zokni] += 0.5
        except:
            zokni_szotar[zokni] = 0.5
    osszes_par = 0
    for ertek in zokni_szotar.values():
        osszes_par += int(ertek)
    return osszes_par

#by Dwarf
def pair_of_socks7(text:str) -> int:

    zokni_tipusok = set(text)
    osszes_par = 0
    for tipus in zokni_tipusok:
        osszes_par +=text.count(tipus)>>1
    return osszes_par

#by Dwarf
def pair_of_socks8(text:str) -> int:
    paratlan_zokni_tipusok = set()
    osszes_par = 0
    for zokni in text:
        if zokni in paratlan_zokni_tipusok:
            osszes_par += 1
            paratlan_zokni_tipusok.remove(zokni)
        else:
            paratlan_zokni_tipusok.add(zokni)
    return osszes_par

#by Dwarf
def pair_of_socks9(text:str) -> int:

    zokni_szotar = {}
    osszes_par = 0
    for zokni in text:
        if zokni in zokni_szotar:
            del zokni_szotar[zokni]
            osszes_par += 1
        else:
            zokni_szotar[zokni] = 1
    return osszes_par

#by attyhor
def pair_of_socks10(text:str) -> int:
 
    sorttext = sorted(text)
    count = 0
    i = 0
    while i < len(sorttext)-1:
        if sorttext[i] == sorttext[i+1]:
            i += 1
            count += 1
        i += 1
    return count

#by zxld
def pair_of_socks11(text:str) -> int:

    zokni_parok = [text.count(zokni) for zokni in set(text)]
    paros = lambda item: item if not item % 2 else item-1 
    return sum([paros(par) // 2 for par in zokni_parok])

# by Iseroo
def pair_of_socks12(text:str) -> int:

    return sum([int(text.count(x)/2) for x in set(text)])


import string
#by kolt
def pair_of_socks13(text:str) -> int:
    d = {}

    for c in string.ascii_uppercase:
        d[c] = 0

    for c in text:
        d[c] += 1

    counter = 0
    for c in d.keys():
        counter += d[c] // 2

    return counter

#by sanyi (modified)
def pair_of_socks14(text:str) -> int:
    sum = 0
    examined = list()
    for c in text:
        if c not in examined:
            examined.append(c)
            countnum = text.count(c)
            sum += countnum  // 2
    return sum