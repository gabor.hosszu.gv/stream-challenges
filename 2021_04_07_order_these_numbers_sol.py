def order_these_numbers(numbers: list) -> list:
    """
    Növekvő sorendbe állítja a numbers listát.

    Args:
        numbers (list): egy lista amely dict-eket tartalmaz. A dict-ek a következő formátumot követik
        {
            "digit": int
            "sign": str ("+","-" vagy nulla esetén "")
        }

    Returns:
        list: a sorrenbe állított lista
    """
    pass



# by sanyi
def order_these_numbers1(numbers: list) -> list:
    numbers.sort(key=lambda x: int(x["sign"]+str(x["digit"])))
    return numbers

#by attyhor
def order_these_numbers2(numbers: list) -> list:
    lista = []
    for i in range(len(numbers)):
        temp = numbers[i]
        lista.append(int(temp.get("sign")+str(temp.get("digit"))))
    lista.sort()
    return lista

#by slapec
def order_these_numbers3(numbers: list) -> list:
    numbers.sort(key=lambda n: -1 * n['digit'] if n['sign'] == '-' else n['digit'])
    return numbers

#by Iseroo
def order_these_numbers4(numbers: list) -> list:
    lista = {}
    for x in numbers:
        lista[f"{x['sign']}{x['digit']}"] = x
    
    keys = list(map(int, list(lista.keys())))
    keys.sort()
    sorted_lista = []
    for x in keys:
        sorted_lista.append(lista[f"{x}"])
    return sorted_lista

# by Dwarf
def order_these_numbers5(numbers: list) -> list:
    szotar = {}
    vissza = []
    for i in range(len(numbers)):
        temp = numbers[i]
        szotar[int(temp.get("sign")+str(temp.get("digit")))] = i
    for key in sorted(szotar):
        vissza.append(numbers[szotar[key]])
    return vissza

#Tesztelés
assert order_these_numbers([{"sign": "-", "digit": 9},{"sign": "-", "digit": 10},{"sign": "", "digit": 0}]) == [{"sign": "-", "digit": 10}, {"sign": "-", "digit": 9},{"sign": "", "digit": 0}]
