import collections

#by grindy
def vote_count1(votes: list) -> str:
    """
    Visszaadja azt az elemet, amely a legtöbbször fordul el a votes listában és
    legalább a votes lista elemeinek a felét (felfelé kerekítve) teszi ki.
    Ha nincs ilyen akkor None-t ad vissza.  

    Args:
        votes (list): egy lista amelynek elemei stringek

    Returns:
        str: a fenti kritériumoknak megfelelő string, ha nincs ilyen akkor None
    """

    c = collections.Counter(votes)
    len_v = len(votes)
    for i in c:
        if c[i] >= len_v / 2:
            return i
    return None
    
#by papi1992
def vote_count2(votes: list) -> str:
    for vote in set(votes):
        if votes.count(vote) / len(votes) >= 0.5:
            return vote
    return None

#by slapec
def vote_count3(votes: list) -> str:

    population = collections.Counter(votes)
    (most_common_vote, most_common_population), = population.most_common(1)
    if most_common_population > len(votes) / 2:
        return most_common_vote

#by kolt
def vote_count4(votes:list) -> str:
    elements = set(votes)
    n = max((votes.count(element),element) for element in elements)
    return n[1] if n[0] >= round(len(votes) // 2) else None

#by grindy
def vote_count5(votes):
    vote_len = len(votes)
    vote_dict = {}
    for vote in votes:
        if vote not in vote_dict:
            vote_dict[vote] = 1
        else:
            vote_dict[vote] += 1
        
        if vote_dict[vote] >= vote_len/2:
            return vote