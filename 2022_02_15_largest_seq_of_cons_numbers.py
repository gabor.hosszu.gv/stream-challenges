def get_seq(digits: list[int]) -> list[int]:
    """
    Kikeresi a kapott számsorból a leghosszabb folyamatos sorozatot, majd 
    listaként visszaadja a legkisebb és legnagyobb értékét.
    Ha több egyenlő hosszúságú számsor van, a nagyobb összértékűt adja vissza.
    Amennyiben nincs folyamatos sorozatot None-t ad vissza

    Példa:
    [4,1,-1,2,7,3,6] => [1,4]

    Args:
        digits (list[int]): ismétlés nélküli, rendezetlen számsor, amely legalább 2 elemet tartalmaz

    Returns:
        list[int]: a megtalált számsorozat legkisebb és legynagyobb értéke növekvő sorrendben vagy None
    """
    
# Testing starts here
test_data = [
    {
    'args': [[-62, 43]],
    'result': None
},
{
    'args': [[1,2,3,4,6]],
    'result': [1,4]
},
{
    'args': [[8, 0, 1, 9, 5, 15, 3, 2, 4, 12]],
    'result': [0,5]
},
{
    'args': [[9, -4, -7, 4, 6, 7, 1, 8, 2, 5, -3, -2]],
    'result': [4, 9]
},
{
    'args': [[10, 15, 13, -19, -5, -11, 4, 20, -7, -12, -16, -15, -6, 1, 5, 14, 3, -1, -17, 19, 0, 16, -2, 11, -9, -13, 7, -14, 2, 12]],
    'result': [-2,5]
},
{
    'args': [[-62, 43, -53, -23, 37, -9, -22, 49, -1, 80, 98, -29, 44, -90, 3, 55, -3, 52, -59, -88, -86, 72, 1, 39, 35, 92, 56, -65, 7, -61, -16, 27, 66, -30, -100, 4, 8, -51, 45, 30, -42, 9, 0, 76, -2, 87, -81, 99, 90, -12, 41, 65, -99, 24, -13, -70, -45, -33, 71, 38, -83, 97, -63, 83, -79, 2, 22, -15, -54, -82, 84, 28, -98, 12, 53, -66, 46, -40, -96, -5, 23, 82, -28, 95, 78, 14, 96, -50, -11, 32, 58, 74, -94, 25, -72, 73, 79, -25, 62, 5]],
    'result': [-3,5]
},
]

for data in test_data:
    assert get_seq(*data['args']) == data["result"]

print("All test OK", end='\n\n')
