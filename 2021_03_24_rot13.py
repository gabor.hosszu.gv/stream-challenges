def rot13(message: str) -> str:
    """
    Case-sensitive Rot13 kódoló
    Ro13 kód: https://hu.wikipedia.org/wiki/ROT13
    Tilos az 'encode' használata

    Args:
        message (str): Kódolásra való üzenet

    Returns:
        str: Az eredeti üzenet Rot13 szerint kódolva
    """
