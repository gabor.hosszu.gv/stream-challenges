def byte_reverse_1(bytes):
    # grindy 
    result = []
    for i in range(len(bytes) // 8):
        result = bytes[i*8:i*8+8] + result
    return result
    
def byte_reverse_2(bytes):
    # dwarf I
    vissza_byte= []
    for byte in range(len(bytes)>>3):
        for bit in range (8,0,-1):
            vissza_byte.append(bytes[len(bytes)-(byte<<3)-bit])
    return (vissza_byte)

def byte_reverse_3(bits):
    # slapec
    retval = []
    for window in range(len(bits), 0, -8):
        retval += bits[window-8:window]

    return retval

def byte_reverse_4(bytes):
    # dwarf II
    vissza_byte= []
    for byte in range(len(bytes),0,-8):
        for bit in range (8,0,-1):
            vissza_byte.append(bytes[byte-bit])
    return (vissza_byte)

def byte_reverse_5(bytes):
    # dwarf III
    return ((bytes) if len(bytes)==8 else (bytes[-8:]+byte_reverse_5(bytes[:-8])))

def byte_reverse_6(bytes):
    # dwarf IV
    import math
    if len(bytes) == 8:
        return (bytes)
    vissza_bytes = []
    szam = int(''.join(map(str, bytes)), 2)
    hatvany = int(math.log(szam, 256))+1
    for i in range (hatvany):
        szam, maradek = divmod (szam, 256)
        kov_byte =  [int (bit) for bit in list(bin(maradek)[2:])]
        hiany = 8 - len(kov_byte)
        for i in range(hiany) :
            kov_byte = [0] + kov_byte
        vissza_bytes = vissza_bytes + kov_byte
    return vissza_bytes

def byte_reverse_7(bytes):
    # dwarf V
    if len(bytes) == 8:
        return (bytes)
    vissza_bytes = []
    szam = int(''.join(map(str, bytes)), 2)
    while szam:
        szam, maradek = divmod (szam, 256)
        kov_byte =  [int (bit) for bit in list(bin(maradek)[2:])]
        hiany = 8 - len(kov_byte)
        for i in range(hiany) :
            kov_byte = [0] + kov_byte
        vissza_bytes = vissza_bytes + kov_byte
    return vissza_bytes

def byte_reverse_8(bytes):
    # HBXSZA
    num_of_bits = len(bytes)

    for i in range(0, num_of_bits // 16 * 8, 8):
        mirror = num_of_bits - i
        i_plus_8 = i + 8
        mirror_minus_8 = mirror - 8
        bytes[i : i_plus_8], bytes[mirror_minus_8 : mirror] = bytes[mirror_minus_8 : mirror], bytes[i : i_plus_8]
        
    return bytes

def byte_reverse_9(bytes):
    # kolt
    result_list = []
    for start_pos in range(len(bytes)-1, 0, -8):
        result_list += bytes[start_pos-7:start_pos+1]

    return(result_list)

def byte_reverse_10(bytes):
    # Hels
    done_bytes = []

    if len(bytes) == 8:
       return bytes

    else:
        counter = 0
        bytes_list = []
        current_bytes = []
        for byte in bytes:
            counter += 1
            current_bytes.append(byte)
            if counter == 8:
                bytes_list.append(current_bytes)
                current_bytes = []
                counter = 0
                
         # reverse list
        bytes_list.reverse()
        for byte_list in bytes_list:
            for byte in byte_list:
                done_bytes.append(byte)
        return done_bytes

def byte_reverse_11(bytes):
    # HBXSZA II
    from more_itertools import chunked, flatten
    return [*flatten([*chunked(bytes, 8)][::-1])]

def byte_reverse_12(bytes):
    # HBXSZA III
    return [bytes[i-8+j] for i in range(len(bytes), 0, -8) for j in range(8)]
