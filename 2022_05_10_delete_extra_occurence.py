def delete_extra_occurence(order: list, max_occ: int) -> list:
    """
    Módosítja a kapott order listát, úgy, hogy egyetelen eleme
    sem fordulhat elő többször, mint a max_occ. Az előfordulásokat
    sorrendet követve törli

    Példa:
    ([20,37,20,21], 1) => [20,37,21]
    ([1,1,3,3,7,2,2,2,2], 3) => [1, 1, 3, 3, 7, 2, 2, 2]

    Args:
        order [list[int]]
        max_occ [int]

    Returns:
        list: a módosított lista
    """

# Testing starts here
test_data = [
    {
        "args": [[20, 37 ,20 ,21], 1],
        "result": [20, 37, 21]
    },
    {
        "args": [[5, 5, 4, 4, 3, 5, 4, 1], 2],
        "result": [5, 5, 4, 4, 3, 1]
    },
    {
        "args": [[1, 1, 3, 3, 7, 2, 2, 2, 2], 3],
        "result": [1, 1, 3, 3, 7, 2, 2, 2]
    },
    {
        "args": [[4, 7, 10, 1, 8, 7, 7, 4, 3, 1], 2],
        "result": [4, 7, 10, 1, 8, 7, 4, 3, 1]
    },
    {
        "args": [[2, 10, 8, 2, 2, 5, 7, 1, 2, 6, 2, 10, 1, 10, 2], 3],
        "result": [2, 10, 8, 2, 2, 5, 7, 1, 6, 10, 1, 10]
    },
    {
        "args": [[9, 6, 4, 4, 1, 2, 9, 4, 6, 9, 9, 2, 6, 7, 4, 2, 2, 4, 2, 3], 2],
        "result": [9, 6, 4, 4, 1, 2, 9, 6, 2, 7, 3]
    },
    {
        "args": [[1, 7, 5, 5, 5, 2, 7, 10, 8, 5, 1, 7, 4, 5, 2, 5, 8, 9, 6, 7, 8, 4, 2, 1, 9, 7, 1, 6, 4, 6], 4],
        "result": [1, 7, 5, 5, 5, 2, 7, 10, 8, 5, 1, 7, 4, 2, 8, 9, 6, 7, 8, 4, 2, 1, 9, 1, 6, 4, 6]
    },
        {
        "args": [[13, 17, 16, 8, 16, 11, 15, 15, 4, 10, 15, 19, 3, 1, 4, 6, 2, 16, 18, 9, 7, 14, 3, 6, 16, 7, 13, 1, 17, 1, 15, 11, 18, 13, 9, 3, 18, 11, 4, 1, 10, 14, 20, 18, 20, 14, 1, 16, 15, 8], 5],
        "result": [13, 17, 16, 8, 16, 11, 15, 15, 4, 10, 15, 19, 3, 1, 4, 6, 2, 16, 18, 9, 7, 14, 3, 6, 16, 7, 13, 1, 17, 1, 15, 11, 18, 13, 9, 3, 18, 11, 4, 1, 10, 14, 20, 18, 20, 14, 1, 16, 15, 8]
    },
]

for data in test_data:
    assert delete_extra_occurence(*data["args"]) == data["result"]

print("All test OK", end="\n\n")
