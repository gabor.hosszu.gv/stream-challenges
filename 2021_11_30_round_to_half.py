def round_to_half(number: float) -> float:
    """
    Felfelé vagy lefelé kerekíti a számot 0.5 lépték szerint

    Példa a működésre:
        4.2     =>  4
        4.25    =>  4.5
        4.6     =>  4.5
        4.75    =>  5

    Args:
        number: A kerekítésre váró szám

    Returns:
        float: A kerekítés eredménye
    """

# Testing starts here
test_data = [
{
    'args': [4.2],
    'result': 4
},
{
    'args': [4.25],
    'result': 4.5
},
{
    'args': [4.4],
    'result': 4.5
},
{
    'args': [4.6],
    'result': 4.5
},
{
    'args': [4.75],
    'result': 5
},
{
    'args': [4.8],
    'result': 5
},
]

for data in test_data:
    assert round_to_half(*data['args']) == data["result"]

print("All test OK", end='\n\n')
