# Ez a megoldás csak python 3.8+ verzóknál működik, 2 okból
# 3.6 előtti verziók nem garantálják hogy a dict tartja a sorrendet
# 3.8-ban vezették be a 9. sorban található walrus operátort ( := )
dic = {1000:"M", 900:"CM", 500:"D", 400:"CD", 100:"C", 90:"XC", 50:"L", 40:"XL", 10:"X", 9:"IX", 5:"V", 1:"I"}

def int_to_roman(n: int) -> str:
    result = ''
    for d in dic:
        if (new:=n // d) > 0:
            result += new*dic[d]
            n -= new*d
    return result
