def anti_list(list1:list, list2:list) -> bool:
    """
    Ez a függyvény megvizsgálja, hogy a két lista antilista-e.
    Két lista akkor antilistája egymásnak, ha:
    - ugyanolyan hosszúak
    - ugyanabból a két elemből állnak
    - elemeik pont forítva vannak a másikéhoz képes

    Példa:
    anti_list(["1", "0", "0", "1"], ["0", "1", "1", "0"]) -> True
    anti_list([3.14, True, 3.14], [3.14, False, 3.14]) -> False

    Args:
        list1, list2 (list): Legfeljebb két különböző elemet tartalmazó, rendezetlen lista.
            Elemei lehetnek stringek, integerek, floatok vagy boolok.
            Legalább egy elemet tartalmaznak.

    Returns:
        bool: True ha anitlistákról van szó, minden más esetben False
    """

# Testing starts here
test_data = [
    {
     'args' : [["1", "0", "0", "1"], ["0", "1", "1", "0"]],
     'result' : True
    },
    {
     'args' : [["apples", "bananas", "bananas"], ["bananas", "apples", "apples"]],
     'result' : True
    },
    {
     'args' : [[3.14, True, 3.14], [3.14, False, 3.14]],
     'result' : False
    },
]

for data in test_data:
    assert anti_list(*data['args']) == data["result"]

print("All test OK", end='\n\n')
