def chess_knight1(cell: str) -> int:
    #grindy
    base = [2,3,4,4,4,4,3,2]
    if int(cell[1])    in (1,8):
        return base[ord(cell[0])-97]
    elif int(cell[1])    in (2, 7):
        return list(map(lambda x: x+x//2, base))[ord(cell[0])-97]
    else: 
        return list(map(lambda x: x*2, base))[ord(cell[0])-97]
"""
def chess_knight2(cell: str) -> int:
    #Dwarf(I)
    tabla = {
        'a' : {
            '1': 2,
            '2': 3,
            '3': 4,
            '4': 4,
            '5': 4,
            '6': 4,
            '7': 3,
            '8': 2
              },
        'b': {
            '1': 3,
            '2': 4,
            '3': 6,
            '4': 6,
            '5': 6,
            '6': 6,
            '7': 4,
            '8': 3
            },
        'c': {
            '1': 4,
            '2': 6,
            '3': 8,
            '4': 8,
            '5': 8,
            '6': 8,
            '7': 6,
            '8': 4
        },
        'd': {
            '1': 4,
            '2': 6,
            '3': 8,
            '4': 8,
            '5': 8,
            '6': 8,
            '7': 6,
            '8': 4
        },
        'e': {
            '1': 4,
            '2': 6,
            '3': 8,
            '4': 8,
            '5': 8,
            '6': 8,
            '7': 6,
            '8': 4
        },
        'f': {
            '1': 4,
            '2': 6,
            '3': 8,
            '4': 8,
            '5': 8,
            '6': 8,
            '7': 6,
            '8': 4
        },
        'g': {
            '1': 3,
            '2': 4,
            '3': 6,
            '4': 6,
            '5': 6,
            '6': 6,
            '7': 4,
            '8': 3
        },
        'h': {
            '1': 2,
            '2': 3,
            '3': 4,
            '4': 4,
            '5': 4,
            '6': 4,
            '7': 3,
            '8': 2
            },
    }
    return tabla[cell[0]][cell[1]]

"""
"""
def chess_knight3(cell: str) -> int:
    #HBXSZA
    if cell == 'a1': return 2
    if cell == 'a8': return 2
    if cell == 'h1': return 2
    if cell == 'h8': return 2
    if cell == 'a2': return 3
    if cell == 'a7': return 3
    if cell == 'h2': return 3
    if cell == 'h7': return 3
    if cell == 'b1': return 3
    if cell == 'b8': return 3
    if cell == 'g1': return 3
    if cell == 'g8': return 3
    if cell == 'a3': return 4
    if cell == 'a4': return 4
    if cell == 'a5': return 4
    if cell == 'a6': return 4
    if cell == 'h3': return 4
    if cell == 'h4': return 4
    if cell == 'h5': return 4
    if cell == 'h6': return 4
    if cell == 'c1': return 4
    if cell == 'c8': return 4
    if cell == 'd1': return 4
    if cell == 'd8': return 4
    if cell == 'e1': return 4
    if cell == 'e8': return 4
    if cell == 'f1': return 4
    if cell == 'f8': return 4
    if cell == 'b2': return 4
    if cell == 'b7': return 4
    if cell == 'g2': return 4
    if cell == 'g7': return 4
    if cell == 'b3': return 6
    if cell == 'b4': return 6
    if cell == 'b5': return 6
    if cell == 'b6': return 6
    if cell == 'g3': return 6
    if cell == 'g4': return 6
    if cell == 'g5': return 6
    if cell == 'g6': return 6
    if cell == 'c2': return 6
    if cell == 'c7': return 6
    if cell == 'd2': return 6
    if cell == 'd7': return 6
    if cell == 'e2': return 6
    if cell == 'e7': return 6
    if cell == 'f2': return 6
    if cell == 'f7': return 6
    if cell == 'c3': return 8
    if cell == 'c4': return 8
    if cell == 'c5': return 8
    if cell == 'c6': return 8
    if cell == 'd3': return 8
    if cell == 'd4': return 8
    if cell == 'd5': return 8
    if cell == 'd6': return 8
    if cell == 'e3': return 8
    if cell == 'e4': return 8
    if cell == 'e5': return 8
    if cell == 'e6': return 8
    if cell == 'f3': return 8
    if cell == 'f4': return 8
    if cell == 'f5': return 8
    if cell == 'f6': return 8
"""
def chess_knight4(cell: str) -> int:
    #slapec
    # A huszár lépései beleférnek egy 5x5-ös gridbe
    # Egy bitmaskben kódolom, hogy a huszár milyen pozíciókra léphet
    #
    # | |X| |X| |
    # |X| | | |X|
    # | | |H| | |
    # |X| | | |X|
    # | |X| |X| |
    #
    # A pozíciókból sorfolytonosan képzem a maszkot
    # Ha a huszár a grid széléhez közeledik, akkor a gridből kieső
    # pozíciók nullás értéket kapnak a maszkban
    
    masks = {
        'a': 85,   # 01010101
        'b': 215,  # 11010111
        'g': 235,  # 11101011
        'h': 170,  # 10101010
        '1': 240,  # 11110000
        '2': 252,  # 11111100
        '7': 63,   # 00111111
        '8': 15    # 00001111
    }

    x, y = cell
    # A két maszkot logikai AND kapcsolatba hozzuk, így egy olyan maskot kapunk
    # eredényül, amiben az egyes számjegyek a gridben maradó pozíciókat kódolják.
    # Ez után csak meg kell számolni az egyesek mennyiségét a bináris számban
    # (Ha a grid közepén van a huszár, akkor a maszk csupa egyes számjegyeket tartalmaz.
    # Ezeket nem akartam f a masks dictben, helyettük a dict.get() gondoskodik róluk )
    return format(masks.get(x, 255) & masks.get(y, 255), '08b').count('1')

def chess_knight5(cell: str) -> int:
    #HBXSZA II
    x, y = ord(cell[0]) - 96, int(cell[1])
    return len([True for i, j in zip((1, 1, 2, 2, -1, -1, -2, -2), (-2, 2, -1, 1) * 2) if x+i in range(1, 9) and y+j in range(1, 9)]) 

def chess_knight6(cell: str) -> int:
    #Dwarf(II)
    oszlopérték = {
        'a' : 1,
        'b' : 2,
        'c' : 3,
        'd' : 4,
        'e' : 4,
        'f' : 3,
        'g' : 2,
        'h' : 1
    }
    sorérték  = {
        '1' : 1,
        '2' : 2,
        '3' : 3,
        '4' : 4,
        '5' : 4,
        '6' : 3,
        '7' : 2,
        '8' : 1
    }
    ertek = oszlopérték[cell[0]]  * sorérték[cell[1]]
    if ertek > 8:
        return 8
    if ertek > 5:
        return 6
    if ertek > 2:
        return 4
    if ertek > 1:
        return 3
    return 2

def chess_knight7(cell: str) -> int:
    #HBXSZA III
    global __knight_dict__
    if '__knight_dict__' in globals():
        return __knight_dict__[cell]

    def _chess_knight(cell: str) -> int:
        x, y = ord(cell[0]) - 96, int(cell[1])
        return len([True for i, j in zip((1, 1, 2, 2, -1, -1, -2, -2), (-2, 2, -1, 1) * 2) if x+i in range(1, 9) and y+j in range(1, 9)])
    
    __knight_dict__ = {(arg := i+str(j)): _chess_knight(arg) for i in ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h') for j in range(1, 9)}
    
    return __knight_dict__[cell]
    
