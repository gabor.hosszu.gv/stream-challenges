#by grindy
def sum_of_missing1(nums:list) -> int:
    return sum(set(range(min(nums), max(nums)+1)).difference(set(nums)))

#by grind2
def sum_of_missing2(nums:list) -> int:
    result = 0
    nums.sort()
    for i in range(len(nums)-1):
        if nums[i]+1 != nums[i+1]:
            result += sum(range(nums[i]+1, nums[i+1]))
    return result

#by nyedu
def sum_of_missing3(nums:list) -> int:
    nums.sort()
    missings = []
    for x in nums:
        if x+1 not in nums and not x == nums[-1]:
            missings.append(x+1)
            nums.append(x+1)
            nums.sort()
    return sum(missings)

#by slapec
def sum_of_missing4(nums: list[int]) -> int:

    nums.sort()
    return sum(set(range(nums[0], nums[-1])) - set(nums))

#by slapec
def sum_of_missing5(nums: list[int]) -> int:

    nums.sort()
    total = 0

    for current_value, next_value in zip(nums, nums[1:]):
        delta = next_value - current_value
        if delta > 1:
            delta_sum = (delta - 1) * (current_value + next_value) // 2
            total += delta_sum
    return total

#by slapec
def sum_of_missing6(nums: list[int]) -> int:
    nums.sort()
    return ((nums[0] + nums[-1]) * (nums[-1] - nums[0] + 1)) // 2 - sum(nums)

#by Dwarf
def sum_of_missing7(nums:list) -> int:
    kezdo = min(nums)
    vegso = max(nums)
    hianyzok_osszege = ((kezdo+vegso)*(vegso-kezdo+1))//2
    for szam in nums:
        hianyzok_osszege-= szam
    return hianyzok_osszege

#by Dwarf
def sum_of_missing8(nums:list) -> int:

    kezdo = nums[0]
    vegso = nums[0]
    osszeg = 0
    for szam in nums:
        osszeg+= szam
        if szam < kezdo:
            kezdo = szam
        elif szam > vegso:
            vegso = szam
    teljes_osszeg = ((kezdo+vegso)*(vegso-kezdo+1))//2
    return teljes_osszeg-osszeg

#by Dwarf
def sum_of_missing9(nums:list) -> int:
    return ((min(nums)+max(nums))*(max(nums)-min(nums)+1))//2-sum(nums)

#by Máté
def sum_of_missing10(nums:list):
    nums.sort()
    return sum(set(range(min(nums), max(nums)+1))-set(nums))

#by kolt
def sum_of_missing11(nums:list) -> int:
    nums_set = set(nums)
    whole_set = set(range(min(nums_set),max(nums_set),1))
    return sum(whole_set - nums_set)

#by kolt
def sum_of_missing12(nums:list) -> int:
    nums_set = set(nums)
    sorted_nums = sorted(nums)
    whole_set = set(range(sorted_nums[0],sorted_nums[len(sorted_nums)-1],1))
    return sum(whole_set - nums_set)

#by attyhor
def sum_of_missing13(nums:list) -> int:
    maximum = max(nums)
    minimum = min(nums)
    szumma = 0
    for i in range(minimum, maximum):
        if i not in nums:
            szumma += i
    return szumma

#by sanyi
def sum_of_missing14(nums: list) -> int:
    return sum({*range(min(nums), max(nums))} - set(nums))

#by kolt
def sum_of_missing15(nums:list):
    max_num = max(nums)
    min_num = min(nums)
    sum_nums = sum(nums)
    every_nums = list(range(min_num,max_num+1,1))
    sum_every_nums = sum(every_nums)
    return sum_every_nums - sum_nums
