def order_these_numbers(numbers: list) -> list:
    """
    Növekvő sorendbe állítja a numbers listát.

    Args:
        numbers (list): egy lista, amely dict-eket tartalmaz. A dict-ek a következő formátumot követik
        {
            "digit": int
            "sign": str ("+","-" vagy nulla esetén "")
        }

    Returns:
        list: a sorrenbe állított lista
    """

#Tesztelés
assert order_these_numbers([{"sign": "-", "digit": 9},{"sign": "-", "digit": 10},[{"sign": "", "digit": 0}]) == [{"sign": "-", "digit": 10}, {"sign": "-", "digit": 9},{"sign": "", "digit": 0}]
