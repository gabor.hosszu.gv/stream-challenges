def making_progress(days):
    """
    Johnny minden nap dolgozok, de nem minden nap halad.
    A kapott days lista tartalmazza, hogy hogy sikerült egy-egy napja.
    Minél nagyobb értéket kap egy nap, annál sikeresebb.
    Akkor ért el haladást, ha az adott napja jobban sikerül, mint az előző.
    A függvény visszaadja, hogy hányszor ért el haladást az adott napokban.

    Args:
        list ([int]): Mindig legalább 2 elemből áll.

    Returns:
        int: haladások száma
    """


# Testing starts here
test_data = [
    {
        'args' : [[3, 4, 1, 2]],
        'result' : 2
    },
    {
        'args' : [[10, 11, 12, 9, 10]],
        'result' : 3
    },
    {
        'args' : [[6, 5, 4, 3, 2, 9]],
        'result' : 1
    },
        {
        'args' : [[9, 9]],
        'result' : 0
    },
]

for data in test_data:
    assert making_progress(*data['args']) == data["result"]

print("All test OK", end='\n\n')