def sum_frac(fracts: list[list[int,int]]):
    """
    Összeadja a listában listaként megadott törteket.
    Amennyiben az eredmény egész szám, integerként adja vissza, egyébként listaként.

    Példa:
    [[1, 2], [1, 3], [1, 4]] => [13, 12]
    [[1, 3], [5, 3]] => 2

    Args:
        fracts [list[list[int,int]]]: A törteket tartalmazó lista.
            Minden eleme egy 2 elemből álló lista, ami egy törtet jelképez. Első eleme a számláló, a második a nevező.
            Ezek a számok mindig 0-nál nagyobb, egész számok.
    Returns:
        A tört összeadás eredménye
        - ha egész szám, akkor integer,
        - ha tört, akkor lista, amelynek az első eleme a számláló, a második a nevező.
    """
    
# Testing starts here
test_data = [
    {
        'args': [[[1, 3], [5, 3]]],
        'result': 2
    },
    {
        'args': [[[1, 2], [1, 3], [1, 4]]],
        'result': [13, 12]
    },
    {
        'args': [[[10, 6], [2, 3], [8, 8], [1, 8], [3, 8]]],
        'result': [23, 6]
    },
    {
        'args': [[[8, 1], [6, 2], [4, 8], [9, 1], [10, 8], [9, 3], [9, 9], [9, 6], [7, 8], [9, 4]]],
        'result': [243, 8]
    },
    {
        'args': [[[2, 5], [2, 8], [4, 6], [2, 10], [7, 4], [5, 7], [5, 9], [10, 9], [7, 4], [8, 6], [8, 8], [5, 2], [10, 3], [7, 6], [7, 1], [2, 2], [4, 1], [2, 7], [6, 8], [10, 9]]],
        'result': [2779, 90]
    },
    {
        'args': [[[2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2]]],
        'result': 20
    },
]   

for data in test_data:
    assert sum_frac(*data['args']) == data["result"]

print("All test OK", end='\n\n')