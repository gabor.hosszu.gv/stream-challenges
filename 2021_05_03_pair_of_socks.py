def pair_of_socks(text:str) -> int:
    """
    Zoknit válogatni mindenki szeret.
    A kapott text-ben minden karakter egy zoknit jelöl.
    Ez a függyvény visszaadja, hogy hány pár zokni van a text-ben, a pár nélküli zoknikkal nem foglalkozik

    >>> pair_of_socks("AA")
    1

    >>> pair_of_socks("ABABC")
    2

    >>> pair_of_socks("CABBACCC")
    4

    Args:
        text (str): Csupa nagybetűket tartalmazó karakterlánc

    Returns:
        int: Zokinpárok száma
    """


"""
#Tesztelés
import doctest
doctest.testmod()
"""
