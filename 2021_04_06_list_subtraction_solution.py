def array_diff(a:list, b:list) -> list:
    """
    Lista "kivonás.
    Elvátolítja a "b" lista összes elemét az "a" listából és visszaadja az így módosított "a" listát.
    Amennyiben "b" lista egyik eleme többször előfordul az "a" listában, az összeset eltávolítja.
    Amennyiben "b" lista egyik eleme nem fordul az "a" listában nem történik semmi.

    Args:
        a (list): A kisebbítendő lista
        b (list): A kivonandó lista

    Returns:
        list: A külömbség lista
    """
    pass

#by Iseroo
def array_diff1(a:list, b:list) -> list:
    return [x for x in a if x not in b]

#by nyedu
def array_diff2(OfficialBevasarloLista:list, KinaiBevasarloLista:list) -> list:
    bevasarloLista = []
    for targy in OfficialBevasarloLista:
        if targy not in KinaiBevasarloLista:
            bevasarloLista.append(targy)
    return bevasarloLista

#by Dwarf (I)
def array_diff3(a:list, b:list) -> list:
    kozos = set(a) & set(b)
    marad = []
    for elem in a:
        if elem not in kozos:
            marad.append(elem)
    return marad

#by Dwarf (II)
def array_diff4(a:list, b:list) -> list:
    kozos = set(b).intersection(a)
    for elem in kozos:
        while True:
            try:
                a.remove(elem)
            except:
                break
    return a

#by grindy
def array_diff(a:list, b:list) -> list:
    return [x for x in a if x not in b]


#Tesztelés
'''assert array_diff([1,2],[1]) == [2], "Not OK"
assert array_diff([1,2],[]) == [1,2], "Not OK"
assert array_diff([1,2,2,2,3],[2]) == [1,3], "Not OK"
assert array_diff([1,2,2,2,3],[2,4]) == [1,3], "Not OK"'''
