import itertools

#by grindy
def lucky_seven1(number:list) -> bool:
    for i in itertools.combinations(number, 3):
        if sum(i) == 7:
            return True
    return False


#by HBXSZA
def lucky_seven2(numbers: list) -> bool:    
    def find_sum(numbers, sum_, terms):
        if terms == 1:
            return sum_ in numbers
        
        for i, num in enumerate(numbers):
            if find_sum(numbers[i+1:], sum_-num, terms-1):
                return True
            
        return False

    return find_sum(numbers, 7, 3)


#by Dwarf 
def lucky_seven3(number:list) -> bool:
    szamok_1 = set(number)
    for szam_1 in szamok_1:
        number.remove(szam_1)
        szamok_2 = number.copy()
        for szam_2 in szamok_2:
            szamok_2.remove(szam_2)
            if 7-szam_1-szam_2 in szamok_2:
                return True
    return False


#by Dwarf
import bisect
def lucky_seven4(number:list) -> bool:
    number.sort()
    index = bisect.bisect_right(number, 3)
    szamok_1 = set(number[:index])
    for szam_1 in szamok_1:
        number.remove(szam_1)
        szamok_2 = number.copy()
        for szam_2 in szamok_2:
            szamok_2.remove(szam_2)
            if 7-szam_1-szam_2 in szamok_2:
                return True
    return False


#by Dwarf
def lucky_seven5(number:list, target_number = 7, used_number_count = 3) -> bool:
    if used_number_count == 1:
        if target_number in number:
            return True
        else:
            return False
    else:
        for szam_1 in number:
            number.remove(szam_1)
            if lucky_seven5(number.copy(), target_number-szam_1, used_number_count-1):
                return True
    return False


#by kolt
def lucky_seven6(number:list) -> bool:
    return True if any([sum(nums) == 7 for nums in itertools.combinations(number, 3)]) else False


#by kolt
def lucky_seven7(number:list) -> bool:
    nums = itertools.combinations(number, 3)
    for _nums in nums:
        if sum(_nums) == 7:
            return True
    return False


'''#Tesztelés
import doctest
doctest.testmod()'''


import timeit
print(f'grindy {str(timeit.timeit("lucky_seven1([2, 4, 3, 8, 9, 1])", globals=globals()))}')
print(f'HBXSZA {str(timeit.timeit("lucky_seven2([2, 4, 3, 8, 9, 1])", globals=globals()))}')
print(f'Dwarf {str(timeit.timeit("lucky_seven3([2, 4, 3, 8, 9, 1])", globals=globals()))}')
print(f'Dwarf {str(timeit.timeit("lucky_seven4([2, 4, 3, 8, 9, 1])", globals=globals()))}')
print(f'Dwarf {str(timeit.timeit("lucky_seven5([2, 4, 3, 8, 9, 1])", globals=globals()))}')
print(f'kolt {str(timeit.timeit("lucky_seven6([2, 4, 3, 8, 9, 1])", globals=globals()))}')
print(f'kolt {str(timeit.timeit("lucky_seven7([2, 4, 3, 8, 9, 1])", globals=globals()))}')