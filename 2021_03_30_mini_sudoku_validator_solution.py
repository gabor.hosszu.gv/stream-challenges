def mini_sudoku_validator(box: list) -> bool:
    """
    Mini sudoku validáló függyvény. Ellenőrzi, hogy a kapott box változó megfelel-e a szabályoknak.
    Szabályok:
            - nincs ismétlődő szám
            - megvan minden szám 1 és 9 között
            - az alakja 3x3

    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 9, 7]])
    True

    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 5, 7]])
    False

    >>> mini_sudoku_validator([[1, 32], [6, 5, 4], [8, 9, 7]])
    False

    Args:
        box (list): 3 elemet tartalmazó lista, ahol minden elem a mini-sudoku egy sora

    Returns:
        bool: True ha megfelel a szabályoknak, minden más esetben False
    """
    pass

    #by attyhor
def mini_sudoku_validator1(box: list) -> bool:
    """
    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 9, 7]])
    True

    
    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 5, 7]])
    False

    >>> mini_sudoku_validator([[1, 32], [6, 5, 4], [8, 9, 7]])
    False
    """
    validalando_list = []
    valid_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    for row in box:
        while len(row) != 3:
            return False
        validalando_list.append(row[0])
        validalando_list.append(row[1])
        validalando_list.append(row[2])
    validalando_list.sort()
    if validalando_list == valid_list:
        return True
    else:
        return False

#by Dwarf
def mini_sudoku_validator2(box: list) -> bool:
    """
    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 9, 7]])
    True

    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 5, 7]])
    False

    >>> mini_sudoku_validator([[1, 32], [6, 5, 4], [8, 9, 7]])
    False
    """
    szamok = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    if len(box) == 3:
        for sor in box:
            if len (sor) == 3:
                for mezo in sor:
                    try:
                        szamok.remove(mezo)
                    except:
                        return(False)
            else:
                return(False)
    else:
        return(False)
    return(True)


#by slapec
def mini_sudoku_validator3(box: list) -> bool:
    """
    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 9, 7]])
    True

    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 5, 7]])
    False

    >>> mini_sudoku_validator([[1, 32], [6, 5, 4], [8, 9, 7]])
    False
    """
    if len(box[0]) == len(box[1]) == len(box[2]) == 3:
        if sorted(box[0] + box[1] + box[2]) == [1, 2, 3, 4, 5, 6, 7, 8, 9]:
            return True
    return False


#by grindy
def mini_sudoku_validator4(box: list) -> bool:
    """
    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 9, 7]])
    True

    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 5, 7]])
    False

    >>> mini_sudoku_validator([[1, 32], [6, 5, 4], [8, 9, 7]])
    False
    """
    if len(box[0]) == len(box[1]) == len(box[2]) == 3:
        str_box = str(box).replace('[', '').replace(']', '').replace(' ', '')
        box_new = list(map(int, (str_box).split(',')))
        return sorted(box_new) == [1, 2, 3, 4, 5, 6, 7, 8, 9]
    return False

#by nyedu

def mini_sudoku_validator5(dobozok:list) -> bool:
    """
    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 9, 7]])
    True

    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 5, 7]])
    False

    >>> mini_sudoku_validator([[1, 32], [6, 5, 4], [8, 9, 7]])
    False
    """
    if len(dobozok) == 3:
        if len(dobozok[0]) == 3:
            if len(dobozok[1]) == 3:
                if len(dobozok[2]) == 3:
                    if sorted(dobozok[0] + dobozok[1] + dobozok[2]) == [1,2,3,4,5,6,7,8,9]:
                        return True
                        #      _.-'''''-._
                        #    .'  _     _  '.
                        #   /   (o)   (o)   \
                        #  |                 |
                        #  |  \           /  |
                        #   \  '.       .'  /
                        #    '.  `'---'`  .'
                        #jgs   '-._____.-'
                    else:
                        return False
                        #     .-""""""-.
                        #   .'          '.
                        #  /   O      O   \
                        # :           `    :
                        # |                |  
                        # :    .------.    :
                        #  \  '        '  /
                        #   '.          .'
                        #jgs  '-......-'
                else:
                    return False
                    #     .-""""""-.
                    #   .'          '.
                    #  /   O      O   \
                    # :           `    :
                    # |                |  
                    # :    .------.    :
                    #  \  '        '  /
                    #   '.          .'
                    #jgs  '-......-'
            else:
                return False
                #     .-""""""-.
                #   .'          '.
                #  /   O      O   \
                # :           `    :
                # |                |  
                # :    .------.    :
                #  \  '        '  /
                #   '.          .'
                #jgs  '-......-'
        else: 
            return False
            #     .-""""""-.
            #   .'          '.
            #  /   O      O   \
            # :           `    :
            # |                |  
            # :    .------.    :
            #  \  '        '  /
            #   '.          .'
            #jgs  '-......-'
    else:
        return False
        #     .-""""""-.
        #   .'          '.
        #  /   O      O   \
        # :           `    :
        # |                |  
        # :    .------.    :
        #  \  '        '  /
        #   '.          .'
        #jgs  '-......-'

  
"""
print(miniSodu([[1,2,3],[4,5,6],[7,8,9]]))
#true
print(miniSodu([[1,2,2],[4,5,5],[7,8,8]]))
#false
print(miniSodu([[6,9,6],[9,6,9],[6,9,6]]))
#false
print(miniSodu([[6,9,6],[9,6,9],[6,9,6],[9,6,9]]))
#false"""


#Tesztelés
import doctest
doctest.testmod()
