def mini_sudoku_validator(box: list) -> bool:
    """
    Mini sudoku validáló függyvény. Ellenőrzi, hogy a kapott box változó megfelel-e a szabályoknak.
    Szabályok:
            - nincs ismétlődő szám
            - megvan minden szám 1 és 9 között
            - az alakja 3x3

    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 9, 7]])
    True

    >>> mini_sudoku_validator([[1, 3, 2], [6, 5, 4], [8, 5, 7]])
    False

    >>> mini_sudoku_validator([[1, 32], [6, 5, 4], [8, 9, 7]])
    False

    Args:
        box (list): 3 elemet tartalmazó lista, ahol minden elem a mini-sudoku egy sora

    Returns:
        bool: True ha megfelel a szabályoknak, minden más esetben False
    """

"""
#Tesztelés
import doctest
doctest.testmod()
"""
