def generate_board(n:int, minimum:int, maximum:int) -> list:
    """
    Memóriatábla generáló a klasszikus játékhoz:
    Generál egy táblát a klasszikus párkeresős memóriajátékhoz, ahol a tábla méretét és
    kártyán szereplő számokat a paraméterek határozzák meg

    Args:
        n (int): tábla hossza és magassága, összesen n*n darab kártya tölti fel
        minimum (int): a legkisebb szám, ami szóbajöhet
        maximum (int): a legnagyobb szám, ami szóbajöhet

    Returns:
        list or None: egy listákat tartalmazó lista, ahol minden elem a tábla egy sora, amennyiben a megoldás lehetséges
    """
