def lucky_seven(number:list) -> bool:
    """
    A kapott numbers listából bármely 3 szám összegeként előállítható-e 7?
    Minden szám csak annyiszor használható fel, ahányoszor szerepel a listában.
    True-t ad vissza ha igen, minden más esetben False

    >>> lucky_seven([2, 4, 3, 8, 9, 1])
    True

    >>> lucky_seven([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    True

    >>> lucky_seven([0, 0, 0, 2, 3])
    False

    >>> lucky_seven([4, 3])
    False

    >>> lucky_seven([1, 8, -2])
    True

    Args:
        number (list): integereket tartalmazó lista (lehetnek negatívak vagy 0 is)

    Returns:
        bool: True ha a 7-es előállítható. Minden más esetben False
    """

"""
#Tesztelés
import doctest
doctest.testmod()
"""
