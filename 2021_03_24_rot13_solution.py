#by Iseroo
def rot131(message: str) -> str: #csak szám és betű, illetve space
    rot = "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm0123456789 "
    abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 "
    return "".join([rot[abc.index(x)] for x in message])

#by slapec
import collections
import string

def rot_n(n):
    lower = collections.deque(string.ascii_lowercase)
    lower.rotate(n)

    upper = collections.deque(string.ascii_uppercase)
    upper.rotate(n)

    translation_map = str.maketrans(string.ascii_uppercase + string.ascii_lowercase, ''.join(upper + lower))

    def rot(message):
        return message.translate(translation_map)

    return rot

rot132 = rot_n(13)

#by grindy
def rot133(message: str) -> str:    
    ALPHABET = "abcdefghijklmnopqrstuvwxyz"
    result = ''
    for c in message:
        if c.isalpha():
            up = c.isupper()
            c = c.lower()
            new_c_pos = ALPHABET.index(c) + 13
            if new_c_pos > len(ALPHABET)-1:
                new_c_pos = new_c_pos - len(ALPHABET)
            if up:
                result += ALPHABET[new_c_pos].upper()
            else:
                result += ALPHABET[new_c_pos]
        else:
            result += c
    return result
