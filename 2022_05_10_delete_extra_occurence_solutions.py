def delete_extra_occurence_1(order: list, max_occ: int) -> list:
    # grindy
    count = {}
    results = []
    for num in order:
        if num in count:
            count[num] += 1
            if count[num] <= max_occ:
                results.append(num)
        else:
            count[num] = 1
            results.append(num)
    return results

def delete_extra_occurence_2(order: list, max_occ: int) -> list:
    # Hels
    res = []
    for item in order:
        if item not in res:
            res.append(item)
        elif res.count(item) + 1 <= max_occ:
            res.append(item)
    return res

def delete_extra_occurence_3(order: list, max_occ: int) -> list:
    # kolt
    dict = {}
    new_order = []

    for num in order:
        if num in dict.keys():
            dict[num] += 1
        else:
            dict[num] = 1
        if dict[num] <= max_occ:
            new_order.append(num)
    return new_order

def delete_extra_occurence_4(order: list, max_occ: int) -> list:
    # Dwarf
    szamok_elofordulas = {}
    visszaad = []
    for szam in order:
        if szam in szamok_elofordulas.keys():
            if szamok_elofordulas[szam] != max_occ:
                szamok_elofordulas[szam] += 1
                visszaad.append(szam)
        else:
            szamok_elofordulas[szam] = 1
            visszaad.append(szam)
    return visszaad

# Testing starts here
test_data = [
    {
        "args": [[20, 37 ,20 ,21], 1],
        "result": [20, 37, 21]
    },
    {
        "args": [[5, 5, 4, 4, 3, 5, 4, 1], 2],
        "result": [5, 5, 4, 4, 3, 1]
    },
    {
        "args": [[1, 1, 3, 3, 7, 2, 2, 2, 2], 3],
        "result": [1, 1, 3, 3, 7, 2, 2, 2]
    },
    {
        "args": [[4, 7, 10, 1, 8, 7, 7, 4, 3, 1], 2],
        "result": [4, 7, 10, 1, 8, 7, 4, 3, 1]
    },
    {
        "args": [[2, 10, 8, 2, 2, 5, 7, 1, 2, 6, 2, 10, 1, 10, 2], 3],
        "result": [2, 10, 8, 2, 2, 5, 7, 1, 6, 10, 1, 10]
    },
    {
        "args": [[9, 6, 4, 4, 1, 2, 9, 4, 6, 9, 9, 2, 6, 7, 4, 2, 2, 4, 2, 3], 2],
        "result": [9, 6, 4, 4, 1, 2, 9, 6, 2, 7, 3]
    },
    {
        "args": [[1, 7, 5, 5, 5, 2, 7, 10, 8, 5, 1, 7, 4, 5, 2, 5, 8, 9, 6, 7, 8, 4, 2, 1, 9, 7, 1, 6, 4, 6], 4],
        "result": [1, 7, 5, 5, 5, 2, 7, 10, 8, 5, 1, 7, 4, 2, 8, 9, 6, 7, 8, 4, 2, 1, 9, 1, 6, 4, 6]
    },
        {
        "args": [[13, 17, 16, 8, 16, 11, 15, 15, 4, 10, 15, 19, 3, 1, 4, 6, 2, 16, 18, 9, 7, 14, 3, 6, 16, 7, 13, 1, 17, 1, 15, 11, 18, 13, 9, 3, 18, 11, 4, 1, 10, 14, 20, 18, 20, 14, 1, 16, 15, 8], 5],
        "result": [13, 17, 16, 8, 16, 11, 15, 15, 4, 10, 15, 19, 3, 1, 4, 6, 2, 16, 18, 9, 7, 14, 3, 6, 16, 7, 13, 1, 17, 1, 15, 11, 18, 13, 9, 3, 18, 11, 4, 1, 10, 14, 20, 18, 20, 14, 1, 16, 15, 8]
    },
]
