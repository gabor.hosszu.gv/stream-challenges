from random import sample, shuffle

def generate_board(n:int, minimum:int, maximum:int) -> list:
    number_of_cards = n*n
    if number_of_cards > (maximum-minimum)*2:
        return None

    cards = sample(range(minimum, maximum+1), number_of_cards // 2) * 2
    shuffle(cards)

    board = []
    for i in range(0, n):
        board.append(cards[i*n:(i+1)*n])

    return board
