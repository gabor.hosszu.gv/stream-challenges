def change_machine(amount: int) -> dict:
    """A megadott amount értéket elváltja magyar forint érmékre és visszaadja úgy, hogy törekedjen a legkevesebb érmére.
    Figyelembeveszi a magyar készképnzes kerekítése szabályokat

    >>> change_machine(456)
    {200: 2, 100: 0, 50: 1, 20: 0, 10: 0, 5: 1}

    Args:
        amount (int): 0 és 999 közötti egész szám

    Returns:
        dict: Az elváltás eredménye az alábbi struktúra szerint
        {
            200: u:int,
            100: v:int,
            50: w:int,
            20: x:int,
            10: y:int,
            5, z:int
        }

    """
    pass


#by slapec
def change_machine(amount: int) -> dict:
    """
    >>> change_machine(456)
    {200: 2, 100: 0, 50: 1, 20: 0, 10: 0, 5: 1}

    >>> change_machine(3)
    {200: 0, 100: 0, 50: 0, 20: 0, 10: 0, 5: 1}

    >>> change_machine(973)
    {200: 4, 100: 1, 50: 1, 20: 1, 10: 0, 5: 1}

    """
    notes = (200, 100, 50, 20, 10, 5)
    retval = {}
    for note in notes:
        note_count = amount // note
        retval[note] = note_count
        amount -= note * note_count

    if amount > 2:
        retval[5] += 1

    return retval


#by grindy
def change_machine2(amount: int) -> dict:
    """
    >>> change_machine2(456)
    {200: 2, 100: 0, 50: 1, 20: 0, 10: 0, 5: 1}

    >>> change_machine2(3)
    {200: 0, 100: 0, 50: 0, 20: 0, 10: 0, 5: 1}

    >>> change_machine2(973)
    {200: 4, 100: 1, 50: 1, 20: 1, 10: 0, 5: 1}

    """
    results = {200:0, 100:0, 50:0, 20:0, 10:0, 5:0}

    for coin in results:
        count = amount // coin
        results[coin] = count
        amount -= coin*count

    if amount > 2:
        results[5] = 1

    return results

#by slapec
def change_machine3(amount: int) -> dict:
    """
    >>> change_machine3(456)
    {200: 2, 100: 0, 50: 1, 20: 0, 10: 0, 5: 1}

    >>> change_machine3(3)
    {200: 0, 100: 0, 50: 0, 20: 0, 10: 0, 5: 1}

    >>> change_machine3(973)
    {200: 4, 100: 1, 50: 1, 20: 1, 10: 0, 5: 1}

    """
    results = {}

    count = amount // 200
    results[200] = count
    amount -= 200*count

    count = amount // 100
    results[100] = count
    amount -= 100*count

    count = amount // 50
    results[50] = count
    amount -= 50*count

    count = amount // 20
    results[20] = count
    amount -= 20*count

    count = amount // 10
    results[10] = count
    amount -= 10*count

    if amount > 2:
        results[5] = 1

    return results

"""import doctest
doctest.testmod()"""
