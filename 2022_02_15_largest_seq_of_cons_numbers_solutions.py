def get_seq_1(digits: list[int]) -> list[int]:
    # HBXSZA I
    digits.sort()
    
    seqs = [[digits.pop(0)]]

    for d in digits:
        last_seq = seqs[-1]
        if d == last_seq[-1] + 1:
            last_seq.append(d)
        else:
            seqs.append([d])

    ret_candidate = max(seqs, key=lambda x: (len(x), x[0]))
    
    if len(ret_candidate) > 1:
        return [ret_candidate[0], ret_candidate[-1]]
    
def get_seq_2(digits: list[int]) -> list[int]:
    # HBXSZA II
    digits.sort()
    digits.append(float('inf'))
    
    max_len = 0
    max_start = float('-inf')
    
    seq = [digits.pop(0)]

    for d in digits: 
        if d == seq[-1] + 1:
            seq.append(d)
        else:
            len_seq = len(seq)
            if len_seq >= max_len:
                max_len = len_seq
                max_start = seq[0]
                max_end = seq[-1]
            seq = [d]
            
    if max_start != max_end:
        return [max_start, max_end]

def get_seq_3(digits: list[int]) -> list[int]:
    # HBXSZA III
    
    digits_dict = {d: [] for d in digits}

    for d in digits:
        if d in digits_dict:
            digits_dict[d].append(d)
            d_inc = d + 1

            while d_inc in digits_dict:
                if not digits_dict[d_inc]:
                    digits_dict[d].append(d_inc)
                    digits_dict.pop(d_inc)
                    d_inc += 1
                else:
                    digits_dict[d].extend(digits_dict.pop(d_inc))
                    break
    
    ret_candidate = max(digits_dict.values(), key=lambda x: (len(x), x[0]))
    
    if len(ret_candidate) > 1:
        return [ret_candidate[0], ret_candidate[-1]]

def get_seq_4(digits):
    # slapec I
    digits.sort()
    ranges = {d: [d, d] for d in digits}
    longest_range = ranges[digits[0]]
    for d, range_ in ranges.items():
        d_next = d + 1
        if d_next in ranges:
            range_.append(d_next)
            ranges[d_next] = range_
            if len(range_) > len(longest_range):
                longest_range = range_

    min_, *rest, max_ = longest_range
    if min_ != max_:
        return [min_, max_]

def get_seq_5(digits):
    # slapec 2
    digits.sort()
    r0 = r1 = x0 = x1 = digits[0]
    s0 = s1 = 0
    for d in digits:
        if d == r1 + 1:
            r1 = d
            s0 += 1

        else:
            if s0 > s1:
                x0 = r0
                x1 = r1
                s1 = s0

            r0 = d
            r1 = d
            s0 = 0

    if s1 or s0:
        if s1 > s0:
            return [x0, x1]
        elif s0 >= s1:
            return [r0, r1]

def get_seq_6(digits: list[int]) -> list[int]:
    # kolt
    digits.sort()
    for seq_len in range(len(digits), 1 , -1):
        for pos_in_seq in range(len(digits) - 1, -1, -1):
            if digits[pos_in_seq - seq_len + 1] == digits[pos_in_seq] - seq_len + 1:
                return [digits[pos_in_seq - seq_len + 1], digits[pos_in_seq]]

def get_seq_7(digits: list[int]) ->list[int]:
    # slapec 3
    digits.sort()
    window_start = window_start_max = digits[0]
    window_size = max_window_size = 1
    for digit in digits:
        if digit == window_start + window_size:
            window_size += 1

        else:
            if window_size > max_window_size:
                window_start_max = window_start
                max_window_size = window_size

            window_start = digit
            window_size = 1

    if window_size > 1 or max_window_size > 1:
        if max_window_size > window_size:
            return [window_start_max, window_start_max + max_window_size - 1]
        elif window_size >= max_window_size:
            return [window_start, window_start + window_size - 1]


from more_itertools  import powerset
def get_seq_8(digits):
    # HBXSZA IV
    return None if not (ret := [[s[0], s[-1]] for subset in powerset(digits) if len(subset) > 1 and (s := sorted(subset)) == [*range(s[0], s[-1]+1)]]) else ret[-1]

def get_seq_9(digits: list[int]):
    # Dwarf
    digits.sort(reverse=True)
    felso_max = digits[0]
    hossz = 1
    felso_temp = digits[0]
    hossz_temp = 1
    for digit in range(1, len(digits)):
        if digits[digit] == felso_temp-hossz_temp:
            hossz_temp += 1
        else:
            if hossz_temp>hossz:
                felso_max = felso_temp
                hossz = hossz_temp
                hossz_temp = 1
                felso_temp = digits[digit]

            else:
                felso_temp = digits[digit]
                hossz_temp = 1

    if hossz_temp>hossz:
        felso_max = felso_temp
        hossz = hossz_temp
    if hossz>1:
        return ([felso_max-hossz+1,felso_max])