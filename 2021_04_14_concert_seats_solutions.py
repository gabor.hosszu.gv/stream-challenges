# original
def can_see_stage(seats:list) -> bool:
    '''
    Ellenőrzi, hogy mindenki rálát-e a színpadra.
    Akkor lát rá valaki a színpadra, ha az összes előtte ülő kisebb (kisebb szám)

    Példa: can_see_stage([[1,2,2], [3,3,3], [7,6,9]])

        #   SZíNPAD   #

            [1,2,2]   #
            [3,3,3]   # A nézőtér
            [7,6,9]   #

        Eredmény: True

    Tesztek:

    >>> can_see_stage([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    True

    >>> can_see_stage([[1, 0, 0], [1, 1, 1], [2, 2, 2]])
    False

    >>> can_see_stage([[1, 2, 2], [2, 3, 2], [7, 8, 9]])
    False

    Args:
        seats (list): Minden eleme egy lista, amik a nézőtér sorait jelképzeik.
            A 0. indexű sor van a legközelebb a színpadhoz.
            A sorokat jelképező lista elemei integerek.
            Minden sor ugyanannyi elemből áll.

    Returns:
        bool: True ha mindenki rálát a színpadra, minden más esetben False
    '''
    pass

#by grindy with list comp
def can_see_stage1(seats:list) -> bool:
    for i in range(len(seats[0])):
        column = [x[i] for x in seats]
        sorted_col = sorted(column)
        if column != sorted_col or len(column) != len(set(sorted_col)):
            return False
    return True

#by grindy
def can_see_stage2(seats:list) -> bool:
    for i in range(len(seats[0])):
        for j in range(len(seats)):
            if j != 0:
                if seats[i][j] <= seats[i][j-1]:
                    return False
    return True

#by grindy with numpy
import numpy as np
def can_see_stage3(seats:list) -> bool:
    # transzponált
    matrix = np.transpose(np.array(seats))
    for line in matrix:
        for i in range(len(line)):
            if i != 0:
                if line[i] <= line[i-1]:
                    return False
    return True

#by Máté
def can_see_stage4(seats:list):
    for row in range(len(seats)):
        if row:
            for seatNum in range(len(seats[row])):
                if seats[row][seatNum]<=seats[row-1][seatNum]:
                    return False
    return True

#by slapec
def can_see_stage5(seats: list[list[int]]) -> bool:
    # Jobban tetszene, ha a színpad felől nézve oszlopokban lennének az
    # ülések, nem pedig sorokban
    seat_columns = zip(*seats)  # Klasszik mátrix transzponálós trükk

    for seat_column in seat_columns:
        # Párban iteráljuk az oszlop üléseit (aktuális, következő ülés)
        for current_seat, next_seat in zip(seat_column, seat_column[1:]):
            if current_seat >= next_seat:
                return False

    return True

#by attyor
def can_see_stage6(seats:list) -> bool:
    sorszam = len(seats)
    flat_list = [item for sublist in seats for item in sublist]
    sorhossz = len(flat_list)
    leptet = int(sorhossz/sorszam)
    i = 0
    while i < sorhossz-leptet*2:
        while flat_list[i] >= flat_list[i+leptet] or flat_list[i+leptet] >= flat_list[i+leptet*2]:
            return False
        i += 1
    return True

#by zxld
def can_see_stage7(seats:list) -> bool:  
    for i in range(len(seats)-2):
        for j in range(len(seats)):
            if not seats[i][j] < seats[i+1][j] < seats[i+2][j]:
                return False
    return True

#by Iseroo
def can_see_stage8(seats:list) -> bool:
    for x in range(len(seats[0])):  
        num = seats[0][x]-1
        for y in range(len(seats)):
            
            if seats[y][x] > num:
                num = seats[y][x]
            else:
                return False
    return True

#by papi1992
def can_see_stage9(seats: list) -> bool:
    for column in zip(*seats):
        for seat, upper_seat in zip(column, column[1:]):
            if seat >= upper_seat:
                return False
    return True

#by nyedu
def can_see_stage10(seats:list) -> bool:
    retValue = True
    for l in range(len(seats)):
        for i in range(len(seats[l])):
            temp_list = []
            for x in range(len(seats)):
                if x+1 <= len(seats):
                    temp_list.append(seats[x][i])
            for x in range(1, len(temp_list)):
                if temp_list[x-1] >= temp_list[x]:
                    retValue = False
    return retValue

#by Dwarf
def can_see_stage11(seats:list) -> bool:
    sorok_szama = len(seats)
    oszlopok_szama = len(seats[0])
    for i in range(oszlopok_szama):
        for j in range(sorok_szama-1):    
            if seats[i][j] > seats[i][j+1]:
                return False
    return True


"""#Tesztelés
import doctest
doctest.testmod()"""




import timeit

test_input_1 = [[1, 2, 2], [2, 3, 2], [7, 8, 9]]
test_input_2 = [[1, 2, 2, 1, 2, 2], [2, 3, 2, 2, 3, 2], [7, 8, 9, 7, 8, 9]]
test_input_3 = [[1, 2, 2], [2, 3, 2], [7, 8, 9],[1, 2, 2], [2, 3, 2], [7, 8, 9],[1, 2, 2], [2, 3, 2], [7, 8, 9]]

test_inputs = [test_input_1,test_input_2,test_input_3]


for input in test_inputs:
    print(f"Timit with: {input}")
    print('grindy list comp ' + str(timeit.timeit(f"can_see_stage1({input})", globals=globals())))
    print('grindy ' + str(timeit.timeit(f"can_see_stage2({input})", globals=globals())))
    print('grindy numpy ' + str(timeit.timeit(f"can_see_stage3({input})", globals=globals())))
    print('Máté ' + str(timeit.timeit(f"can_see_stage4({input})", globals=globals())))
    print('slapec ' + str(timeit.timeit(f"can_see_stage5({input})", globals=globals())))
    print('attyhor ' + str(timeit.timeit(f"can_see_stage6({input})", globals=globals())))
    print('zxld ' + str(timeit.timeit(f"can_see_stage7({input})", globals=globals())))
    print('Iseroo ' + str(timeit.timeit(f"can_see_stage8({input})", globals=globals())))
    print('papi1992 ' + str(timeit.timeit(f"can_see_stage9({input})", globals=globals())))
    print('nyedu ' + str(timeit.timeit(f"can_see_stage10({input})", globals=globals())))
    print('Dwarf ' + str(timeit.timeit(f"can_see_stage11({input})", globals=globals())))

print("Timeit Done!")



