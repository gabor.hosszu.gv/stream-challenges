def can_see_stage(seats:list) -> bool:
    '''
    Ellenőrzi, hogy mindenki rálát-e a színpadra.
    Akkor lát rá valaki a színpadra, ha az összes előtte ülő kisebb (kisebb szám)

    Példa: can_see_stage([1,2,2], [3,3,3], [7,6,9])

        #   SZíNPAD   #

            [1,2,2]   #
            [3,3,3]   # A nézőtér
            [7,6,9]   #

        Eredmény: True

    Tesztek:

    >>> can_see_stage([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    True

    >>> can_see_stage([[1, 0, 0], [1, 1, 1], [2, 2, 2]])
    False

    >>> can_see_stage([[1, 2, 2], [2, 3, 2], [7, 8, 9]])
    False

    Args:
        seats (list): Minden eleme egy lista, amik a nézőtér sorait jelképzeik.
            A 0. indexű sor van a legközelebb a színpadhoz.
            A sorokat jelképező lista elemei integerek.
            Minden sor ugyanannyi elemből áll.

    Returns:
        bool: True ha mindenki rálát a színpadra, minden más esetben False
    '''

'''
#Tesztelés
import doctest
doctest.testmod()
'''