def remove_the_word_1(letters: list, word: str) -> list:
    # grindy
    for c in word:
        try:
            letters.pop(letters.index(c))
        except:
            continue
    return letters

def remove_the_word_2(letters: list, word: str) -> list:
    # HBXSZA
    for letter in word:
        if letter in letters:
            letters.remove(letter)
    return letters

def remove_the_word_3(letters: list, word: str):
    # Dwarf II
    elso_betu = min(letters)
    utolso_betu = max (letters)
    for betu in word:
        if elso_betu <= betu <= utolso_betu:
            if betu in letters:
                letters.remove(betu)
    return letters

def remove_the_word_4(letters: list, word: str) -> list:
    # Máté#2749
    ok = []
    for letter in letters:
        if letter in word:
            word = word.replace(letter, "", 1)
        else:
            ok.append(letter)
    return ok

def remove_the_word_5(letters: list, word: str) -> str:
    # Máté#2749 II
    ok = []
    for letter in letters:
        for letter2 in word:
            if letter == letter2:
                word = word.replace(letter, "", 1)
                break
        else:
            ok.append(letter)
    return ok


def remove_the_word_6(letters: list, word: str) -> list:
    # kolt
    remain_letters = []
    for c in letters:
        if c in word:
            word = word[:word.index(c)] + '*' + word[word.index(c)+1:]
        else:
            remain_letters += c

    return remain_letters

def remove_the_word_7(letters: list, word: str) -> str:
    # Dwarf III

    betuk_elofordulas = {}
    visszaad = []

    for betu in word:
        if betu in betuk_elofordulas.keys():
             betuk_elofordulas[betu] += 1
        else:
            betuk_elofordulas[betu] = 1
    for betu in letters:
        if betu in betuk_elofordulas.keys():
            if betuk_elofordulas[betu] != 1:
                betuk_elofordulas[betu] -= 1
            else:
                del betuk_elofordulas[betu]
        else:
            visszaad.append(betu)
    return visszaad