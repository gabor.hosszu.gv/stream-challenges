def making_progress1(days):
    #grindy
    count = 0
    for i in range(len(days)-1):
        if days[i+1] > days[i]:
            count += 1
    return count


def making_progress2(days):
    #HBXSZA
    return sum(days[i] < days[i+1] for i in range(len(days)-1))


def making_progress3(days):
    #Iseroo
    progress = 0
    for index, x in enumerate(days):
        if index == 0:
            pass
        elif x > days[index-1]:
            progress += 1
    return progress