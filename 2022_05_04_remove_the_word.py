def remove_the_word(letters: list, word: str) -> list:
    """
    A kapott lettes listából eltávolítja azokat a betűket, amelyek
    szerepelnek a word stringben, majd visszaadja a módosított listát.
    Amennyiben egy betű többször is szerepel a listában, csak annyit távolít el
    ahányszor előfordul a word stringben (lásd második példa).
    Az összehasonlítás kis-nagybetű érzékeny.

    Példa:
    (["s", "t", "r", "i", "n", "g", "w"], "string") => ["w"]
    (["b", "b", "l", "l", "g", "n", "o", "a", "w"], "balloon") => ["b", "g", "w"]

    Args:
        letters [list[str]]
        word [str]

    Returns:
        list: a módosított lista
    """


# Testing starts here
test_data = [
    {
        "args": [["s", "t", "r", "i", "n", "g",], ""],
        "result": ["s", "t", "r", "i", "n", "g",]
    },
        {
        "args": [[], "string"],
        "result": []
    },
    {
        "args": [["s", "t", "r", "i", "n", "g", "w"], "string"],
        "result": ["w"]
    },
    {
        "args": [["b", "b", "l", "l", "g", "n", "o", "a", "w"], "balloon"],
        "result": ["b", "g", "w"]
    },
    {
        "args": [["t", "t", "e", "s", "t", "u"], "testing"],
        "result": ["t", "u"]
    },
    {
        "args": [["t", "t", "e", "s", "t", "u"], "testing"],
        "result": ["t", "u"]
    },
    {
        "args": [["t", "1", "e", "S", "t", "!"], "testing"],
        "result": ["1", "S", "!"]
    },
    {
        "args": [["k", "j", "i", "h", "g", "x", "f", "e", "d", "c", "b", "a"], "abcdefghijk"],
        "result": ["x"]
    },
        {
        "args": [['x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x'], "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ"],
        "result": ['x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x']
    },
]

for data in test_data:
    assert remove_the_word(*data["args"]) == data["result"]

print("All test OK", end="\n\n")
