def custom_size_chessboard(number: int) -> list[list[int]]:
    """
    Tetszőleges méretű "sakktábla" generáló.
    Csak 0-t és 1-t tartalmaz, mindig 0-val indul.
    Példa a működésre:
        2    =>  [[0,1],[1,0]]
        3    =>  [[0,1,0],[1,0,1],[0,1,0]]

    Args:
        number: A "sakktábla" sorainak és oszlopainak száma

    Returns:
        list: Az elkészült "sakktábla"
    """

# Testing starts here
test_data = [
{
    'args': [2],
    'result': [[0,1],[1,0]]
},
{
    'args': [3],
    'result': [[0,1,0],[1,0,1],[0,1,0]]
},
{
    'args': [8],
    'result': [[0,1,0,1,0,1,0,1],[1,0,1,0,1,0,1,0],[0,1,0,1,0,1,0,1],[1,0,1,0,1,0,1,0],[0,1,0,1,0,1,0,1],[1,0,1,0,1,0,1,0],[0,1,0,1,0,1,0,1],[1,0,1,0,1,0,1,0]]
},
]

for data in test_data:
    assert custom_size_chessboard(*data['args']) == data["result"]

print("All test OK", end='\n\n')
