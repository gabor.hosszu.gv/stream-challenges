def first_before_second_1(text: str, first: str, second: str) -> bool:
    # grindy
    second_found = False
    for char in text:
        if char == first:
            if second_found:
                return False
        elif char == second:
            second_found = True
    return True

def first_before_second_2(text: str, first: str, second: str) -> bool:
    # Dwarf I 
    if text.rfind(first)<text.find(second):
        return True
    else:
        return False

def first_before_second_3(text: str, first: str, second: str) -> bool:
    # Dwarf II
    if text[text.find(second):].find(first)<0:
        return True
    else:
        return False