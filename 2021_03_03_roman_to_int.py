def roman_to_int1(roman:str) -> int:
    """
    Lefordítja a kapott római számot tizes számrendszerre

    Args:
        roman (str): Egy valid, nagybetűs római szám

    Returns:
        int: A szám tizes számrendszerben
    """
