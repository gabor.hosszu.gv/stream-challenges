def change_machine(amount: int) -> dict:
    """
    A megadott amount értéket elváltja magyar forint érmékre és visszaadja úgy, hogy törekedjen a legkevesebb érmére.
    Figyelembe veszi a magyar készképnezes kerekítése szabályokat

    >>> change_machine(456)
    {200: 2, 100: 0, 50: 1, 20: 0, 10: 0, 5: 1}

    >>> change_machine(3)
    {200: 0, 100: 0, 50: 0, 20: 0, 10: 0, 5: 1}

    >>> change_machine(973)
    {200: 4, 100: 1, 50: 1, 20: 1, 10: 0, 5: 1}

    Args:
        amount (int): pozitív egész szám

    Returns:
        dict: Az elváltás eredménye az alábbi struktúra szerint
        {
            200: u:int,
            100: v:int,
            50: w:int,
            20: x:int,
            10: y:int,
            5, z:int
        }

    """

"""
#Tesztelés
import doctest
doctest.testmod()
"""
