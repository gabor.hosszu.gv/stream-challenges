def chess_knight(cell: str) -> int:
    """
    A huszár egy L-alakban közlekedő sakkfigura
    https://hu.wikipedia.org/wiki/Husz%C3%A1r_(sakk)

    Ez a függvény kiszámolja, hogy hány valid lehetősége van lépni
    a cell pozíción álló huszárnak egy egyébként üres sakktáblán.

    Args:
        cell (str): A huszár figura pozíciója. Mindig 2 karakterből áll, nem kell validálni.
            Első karakter: a-h
            Második karakter 1-8
            Például: "a1", "b4" vagy "h8"

    Returns:
        int: A valid lépések száma a huszár számára egy üres sakktáblán
    """

# Testing starts here
test_data = [
    {
        'args' : ["a1"],
        'result' : 2
    },
    {
        'args' : ["c2"],
        'result' : 6
    },
    {
        'args' : ["d4"],
        'result' : 8
    },
        {
        'args' : ["g6"],
        'result' : 6
    },
]

for data in test_data:
    assert chess_knight(*data['args']) == data["result"]

print("All test OK", end='\n\n')
