def smallest_unique_number(numbers: list) -> integer:
    """
    Ez a függvény megkeresi az adott numbers listában a legkisebb számot, ami csak egyszer fordul elő, tehát nem ismétlődik. Amennyiben nincs
    megoldás -1-t ad vissza.

    Args:
        numbers (list): egy nem-rendezett lista, amely 0-nál nagyobb, egész számokból (integer-ekből) áll.

    Returns:
        int: ha van megoldás akkor a megoldás integer, minden más esetben -1.
    """
