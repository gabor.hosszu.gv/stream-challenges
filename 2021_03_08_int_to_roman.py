def int_to_roman(n: int) -> str:
    """
    A megadott számot római számmá alakítja

    Args:
        n (int): 10-es számrendszerű, 0-nál nagyobb egész szám

    Returns:
        str: A római szám
    """
