import itertools
import re


def same_length1(text:str) -> bool:
    #HBXSZA
    x = [list(j) for _, j in itertools.groupby(text)]
    return not len(x) & 1 and all([len(x[i]) == len(x[i+1]) for i in range(0, len(x), 2)])
    

def same_length2(text:str) -> bool:
    #slapec
    text_it = iter(re.findall(r'0+|1+', text))
    return text.startswith('1') and all(len(ones) == len(zeros) for ones, zeros in itertools.zip_longest(text_it, text_it, fillvalue=''))


def same_length3(text:str) -> bool:
    #Dwarf (I)
    if not text:
        return True
    if 1 > (elso_0_index := text.find("0")) or elso_0_index > len(text)//2:
        return False
    if "1" in text[elso_0_index:2*elso_0_index]:
        return False
    else:
        return same_length3(text[2*elso_0_index:])


def same_length4(text:str) -> bool:
    #Dwarf (II)
    while text and not (len(text) % 2) and ((nulla_helye := text.find("0"))>0) and nulla_helye<=(len(text)//2) and text.find("1")+1:
        text = text[2*(nulla_helye):]
    return not text


def same_length5(text:str) -> bool:
    #Dwarf (III)
    kezdo = 0
    vegso = len(text)//2+1
    while (nulla_helye := text.find("0", kezdo, vegso)-kezdo)>0:
        if "1" in text[nulla_helye+kezdo:2*nulla_helye+kezdo]:
            return False
        kezdo = 2*nulla_helye+kezdo
        vegso = kezdo+len(text[kezdo:])//2+1
    return not text[kezdo:]


def same_length6(text:str) -> bool:
    #Dwarf (IIII)
    kezdet = 1
    le = 0
    for karakter in text:
        if karakter == "1":
            if le and (kezdet - le):
                return False
            kezdet +=1
            le = 0
        else:
            if not kezdet:
                return False
            kezdet -=1
            le = 1
    return kezdet==1

def same_length7(text:str) -> bool:
    #attyhor
    lista = list()
    if text.count("1") != text.count("0"):
        return False
    else:
        char = "0"
        while len(text) != 0:
            result = text.index(char)
            text = text[result:]
            char = "1"
            if len(text) == result:
                return True
            else:
                result2 = text.index(char)
                if result2 != result:
                    return False
                else:
                    text = text[result2:]
            char = "0"
    return True


def same_length8(text: str) -> bool:
    #papi1992
    return all([tp.count('1') == tp.count('0') for tp in re.findall('1*0*', text)])


def same_length9(text:str) -> bool:
    #kolt
    count0 = 0
    count1 = 0
    ok = True
    for c in text:
        if c == '1':
            if count0 > 0:
                ok &= count0 == count1
                if ok:
                    count0 = 0
                    count1 = 0
                else:
                    return False
            count1 += 1
        else:
            count0 += 1
    return count0 == count1


def same_length10(text:str) -> bool:
    #Dwarf(V)
    if ((len(text) % 2)) or text[-1]=="1":
        return False
    text = text.replace('01','0 1').replace('10', '1 0')
    toredekek = text.split(' ')
    for index in range(len(toredekek)//2):
         if len(toredekek[2*index])!=len(toredekek[2*index+1]):
            return False
    return True