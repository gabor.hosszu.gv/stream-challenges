def first_before_second(text: str, first: str, second: str) -> bool:
    """
    Adott egy karakterlánc (text), és két betű (first, second).
    A függvény vizsgálja, hogy a first összes előfordulása a text-be,
    előbb történik, mint a second bármely előfordulása.
    Amennyiben igen, True-t ad vissza. Minden más esetben False.

    Példa:
    ("a rabbit jumps joyfully", "a", "j") => True
    ("knaves knew about waterfalls", "k", "w") => True
    ("precarious kangaroos", "k", "a") => False

    Args:
        text [str] - mindig tartalmazza a first és second karaktereket.
                    Bármilyen hosszú lehet, de legalább 2.
        first [str] - egy karakter
        second [str] - egy karakter
    Returns:
        bool: a vizsgálat eredménye
    """


# Testing starts here
test_data = [
    {
        "args": ["aaaaab", "a", "b"],
        "result": True
    },
    {
        "args": ["aaaaaaaba", "a", "b"],
        "result": False
    },
    {
        "args": ["precarious kangaroos", "k", "a"],
        "result": False
    },
    {
        "args": ["a rabbit jumps joyfully", "a", "j"],
        "result": True
    },
    {
        "args": ["knaves knew about waterfalls", "k", "w"],
        "result": True
    },
    {
        "args": ["aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba", "a", "b"],
        "result": False
    },
    {
        "args": ["baaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba", "a", "b"],
        "result": False
    },
    {
        "args": ["aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab", "a", "b"],
        "result": True
    },
    {
        "args": ["baaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab", "a", "b"],
        "result": False
    },
]

for data in test_data:
    assert first_before_second(*data["args"]) == data["result"]

print("All test OK", end="\n\n")
