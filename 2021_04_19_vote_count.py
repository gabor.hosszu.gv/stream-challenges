def vote_count(votes: list) -> str:
    """
    Visszaadja azt az elemet, amely a legtöbbször fordul el a votes listában és
    legalább a votes lista elemeinek a felét (felfelé kerekítve) teszi ki.
    Ha nincs ilyen akkor None-t ad vissza.  

    Args:
        votes (list): egy lista amelynek elemei stringek

    Returns:
        str: a fenti kritériumoknak megfelelő string, ha nincs ilyen akkor None
    
    >>> vote_count(["A", "A", "B"])
    'A'
    
    >>> vote_count(["A", "A", "A", "B", "C", "A"])
    'A'

    >>> vote_count(["A", "B", "B", "A", "C", "C"]) == None
    True
    """


#Tesztelés
import doctest
doctest.testmod()
