def byte_reverse(bytes):
    """
    Megfordítja az adott byte-ok sorrendjét

    Előtte:
    11111111  00000000  00001111  10101010 

    Utána:
    10101010  00001111  00000000  11111111

    Példa:
    [1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,1,0,1,0,1,0] =>
    [1,0,1,0,1,0,1,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1]

    Args:
        bytes [list[int]]: byte-ok bit-jei, egy lista ami csak 1-et és 0-t tartalmaz.
            Hossza mindig 8-nak többszöröse.

    Returns:
        list[int]: a megfordított byte-ok
    """

    
# Testing starts here
test_data = [
    {
        'args': [[0,0,0,0,0,0,0,0]],
        'result': [0,0,0,0,0,0,0,0]
    },
    {
        'args': [[0,0,1,1,0,1,1,0,0,0,1,0,1,0,0,1]],
        'result': [0,0,1,0,1,0,0,1,0,0,1,1,0,1,1,0]
    },
    {
        'args': [[0,1,0,1,1,0,0,1,1,0,1,1,1,1,1,0,0,0,1,0,0,0,1,1]],
        'result': [0,0,1,0,0,0,1,1,1,0,1,1,1,1,1,0,0,1,0,1,1,0,0,1]
    },
    {
        'args': [[1,1,1,1,0,0,1,0,1,1,0,1,1,0,0,0,0,1,0,0,1,0,0,0,1,1,1,0,1,1,1,1]],
        'result': [1,1,1,0,1,1,1,1,0,1,0,0,1,0,0,0,1,1,0,1,1,0,0,0,1,1,1,1,0,0,1,0]
    },
    {
        'args': [[1,0,0,1,1,0,1,0,1,1,0,1,1,1,1,0,0,0,1,1,0,0,1,1,1,1,0,0,0,0,1,0,1,0,1,1,1,1,1,0]],
        'result': [1,0,1,1,1,1,1,0,1,1,0,0,0,0,1,0,0,0,1,1,0,0,1,1,1,1,0,1,1,1,1,0,1,0,0,1,1,0,1,0]
    },
    {
        'args': [[0,1,0,1,1,0,0,0,1,0,0,0,0,1,0,0,0,1,0,1,1,1,0,1,1,0,1,0,0,0,1,1,1,0,0,0,0,0,1,0,1,1,1,0,1,1,1,1,0,0,1,0,1,1,1,0,1,1,1,1,0,0,1,0,1,0,0,0,1,0,1,0,1,1,1,1,1,1,0,1]],
        'result': [1,1,1,1,1,1,0,1,1,0,0,0,1,0,1,0,1,1,1,1,0,0,1,0,0,0,1,0,1,1,1,0,1,1,1,0,1,1,1,1,1,0,0,0,0,0,1,0,1,0,1,0,0,0,1,1,0,1,0,1,1,1,0,1,1,0,0,0,0,1,0,0,0,1,0,1,1,0,0,0]
    },
    {
        'args': [[1,1,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,0,0,0,1,0,0,1,0,1,0,0,1,1,0,0,1,0,1,0,0,1,0,1,0,1,0,0,1,1,1,0,0,1,0,0,0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,0,1,1,0,0,0,0,1,1,0,0,1,0,0,1,1,1,1,1,0,0,0,0,0,1,0,1,1,0,0,1,0,1,0,0,0,1,1,0,0,0,0,1,0,1,0,1,1,0,1,1,1,0,0,0,1,1,0,1,0,0,0,1,1,1,0,0,1,0,0,0,1,0,1,1,1,0,1,0,1,0,1,1,0,1,0,1,0,0,1,0]],
        'result': [0,1,0,1,0,0,1,0,1,0,1,0,1,0,1,1,1,0,0,0,1,0,1,1,0,0,0,1,1,1,0,0,1,0,0,0,1,1,0,1,0,1,0,1,1,0,1,1,0,1,1,0,0,0,0,1,1,0,0,1,0,1,0,0,0,0,0,0,0,1,0,1,1,0,0,1,1,1,1,1,0,0,0,0,1,1,0,0,0,1,1,1,1,0,1,1,0,1,1,1,1,0,0,0,0,0,1,0,0,0,0,0,1,0,1,0,0,1,1,1,0,1,0,1,0,0,1,0,1,0,1,0,0,1,1,0,0,1,0,0,0,1,0,0,1,1,0,0,0,0,0,0,1,1,0,1,1,1,1,1]
    },
]

for data in test_data:
    assert byte_reverse(*data['args']) == data["result"]

print("All test OK", end='\n\n')