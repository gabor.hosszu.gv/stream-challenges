def odd_row_1(n: int) -> list[int]:
    # grindy I
    start = n*n-n+1
    return [*range(start, start+n*2, 2)]

def odd_row_2(n: int) -> list[int]:
    # grindy II
    return [*range(n*n-n+1,  n*n+1+n, 2)]

def odd_row_3(n: int):
    # Dwarf
    sor = []
    kozep = n*n+1
    for i in range(kozep-n, kozep+n, 2):
        sor.append(i)
    return (sor)

def odd_row_4(n: int):
    # HBXSZA
    mid = n * n + 1
    return [*range(mid-n, mid+n, 2)]
    

def odd_row_5(n: int):
    # slapec
    level = i = 1
    row = []
    while True:
        row.append(i)
        i += 2
        if len(row) == level:
            if level == n:
                return row
            else:
                row = []
                level += 1

def odd_row_6(n: int) -> list[int]:
    # kolt I
    return [x for x in range(n*(n+1)) if x % 2 == 1][-n:]

def odd_row_7(n: int) -> list[int]:
    # kolt II
    return [x for x in range(n*(n-1)+1,n*(n+1)) if x % 2 == 1]